/*
       // *****************************************************
       // * Copyright (c) 2016, IBM Corporation
       // * All rights reserved.
       // *
       // * Redistribution and use in source and binary forms,
       // * with or without modification, are permitted provided
       // * that the following conditions are met:
       // * - Redistributions of source code must retain
       // *   the above copyright notice, this list of conditions
       // *   and the following disclaimer.
       // * - Redistributions in binary form must reproduce the
       // *   above copyright notice, this list of conditions
       // *   and the following disclaimer in the documentation
       // *   and/or other materials provided with the distribution.
       // * - Neither the name of the IBM Corporation nor the names
       // *   of its contributors may be used to endorse or promote
       // *   products derived from this software without specific
       // *   prior written permission.
       // *
       // * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
       // * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
       // * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
       // * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
       // * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
       // * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
       // * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
       // * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
       // * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
       // * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
       // * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
       // * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
       // * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
       // * POSSIBILITY OF SUCH DAMAGE.
       // *****************************************************

  $Id$
*/


#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "php.h"
#include "php_globals.h"
#include "ext/standard/info.h"
#include "ext/standard/php_string.h"
#include "ext/standard/basic_functions.h"

#include "php_ini.h"
#include "php_ile.h"
#include "../virt400/virt400.h"
#include <as400_protos.h>
#include <as400_types.h>

#include <limits.h>
#if ( __WORDSIZE == 64 )
#define PASE_ADDRESS64(x) ((char *)((uint64)x))
#else
#define PASE_ADDRESS64(x) ((char *)((uint32)x))
#endif

#if PHP_MAJOR_VERSION >= 7
#define ZEND_RESOURCE zend_resource
#else
#define ZEND_RESOURCE zend_rsrc_list_entry 
#endif

char * i_lib_default="VLANG";


/* True globals, no need for thread safety */
static int le_result, le_link, le_plink;

ZEND_DECLARE_MODULE_GLOBALS(ile)

static void _free_ile_result(ZEND_RESOURCE *rsrc TSRMLS_DC)
{
}
static void _close_ile_link(ZEND_RESOURCE *rsrc TSRMLS_DC)
{
}
static void _close_ile_plink(ZEND_RESOURCE *rsrc TSRMLS_DC)
{
}
static void _init_ile_globals(zend_ile_globals *ile_globals)
{
	ile_globals->i_lib = i_lib_default;
}

/* {{{ ile_functions[]
 */
static const zend_function_entry ile_functions[] = {
	PHP_FE(ile_connect, NULL)
	PHP_FE(ile_callback, NULL)
	PHP_FE_END
};
/* }}} */

/* {{{ ile_module_entry
 */
zend_module_entry ile_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
	STANDARD_MODULE_HEADER,
#endif
	"ile",
	ile_functions,
	PHP_MINIT(ile),
	PHP_MSHUTDOWN(ile),
	NULL,		/* Replace with NULL if there's nothing to do at request start */
	PHP_RSHUTDOWN(ile),
	PHP_MINFO(ile),
#if ZEND_MODULE_API_NO >= 20010901
	PHP_ILE_VERSION, /* Replace with version number for your extension */
#endif
	STANDARD_MODULE_PROPERTIES
};
/* }}} */


#ifdef COMPILE_DL_ILE
ZEND_GET_MODULE(ile)
#endif


/* {{{ PHP_INI */
PHP_INI_BEGIN()
    STD_PHP_INI_ENTRY("ile.i_lib", NULL, PHP_INI_SYSTEM, OnUpdateString, i_lib, zend_ile_globals, ile_globals)
PHP_INI_END()
/* }}} */


/* {{{ PHP_MINIT_FUNCTION
 */
PHP_MINIT_FUNCTION(ile)
{
	ZEND_INIT_MODULE_GLOBALS(ile, _init_ile_globals, NULL);

	REGISTER_INI_ENTRIES();
	le_result = zend_register_list_destructors_ex(_free_ile_result, NULL, "ile result", module_number);
	le_link = zend_register_list_destructors_ex(_close_ile_link, NULL, "ile link", module_number);
	le_plink = zend_register_list_destructors_ex(NULL, _close_ile_plink, "ile link persistent", module_number);

	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION
 */
PHP_MSHUTDOWN_FUNCTION(ile)
{
	UNREGISTER_INI_ENTRIES();
	return SUCCESS;
}
/* }}} */


/* {{{ PHP_RSHUTDOWN_FUNCTION
 */
PHP_RSHUTDOWN_FUNCTION(ile)
{
	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
 */
PHP_MINFO_FUNCTION(ile)
{
	php_info_print_table_start();
	php_info_print_table_header(2, "ILE support", "enabled");
	php_info_print_table_end();

	DISPLAY_INI_ENTRIES();

}
/* }}} */

#define ROUND_QUAD(x) (((size_t)(x) + 0xf) & ~0xf)
#define VIRT_CALLARG_MAX 32
static void _ile_dump_errorlog(const char * format, ...)
{
    char bigbuff[4096];
    char * p = (char *) &bigbuff;
    va_list args;
    va_start(args, format);
    vsprintf(p, format, args);
    va_end(args);
    php_error_docref(NULL TSRMLS_CC, E_WARNING, p);
}

int _ile_call_script(char * code) {
    int i = 0;
    int ret = 0;
    char *needle = "*debug";
    char lilbuff[1024];
    if (strstr(code, needle)) {
      lilbuff[0] = 0x0;
      for (i=0; i<1024; i++) {
        switch (code[i]) {
        case 0x0:
          lilbuff[i] = code[i];
          i = 1025;
          break;
        case 0x0A:
          lilbuff[i] = 0x20;
          break;
        default:
          lilbuff[i] = code[i];
          break;
        }
      }
      _ile_dump_errorlog("%s",lilbuff);
    }
    zend_try {
    ret = zend_eval_string(code, NULL, (char *)"gen ile block" TSRMLS_CC);
    } zend_end_try();
    return ret;
}

/* {{{ 
*/
PHP_FUNCTION(ile_callback)
{
	double iamd;
	int rc = 0, i = 0;
	int argc = ZEND_NUM_ARGS();
	char *argv[VIRT_CALLARG_MAX];
  char * name = (char *)NULL;
  int nameLen = 0;
  char * outval = (char *)NULL;
	zval * p[VIRT_CALLARG_MAX];
	virtVar_t desc[VIRT_CALLARG_MAX];
	/* up to 64 parms */
	if (zend_parse_parameters(argc TSRMLS_CC, 
	"s|zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz", &name, &nameLen,
	&p[0],&p[1],&p[2],&p[3],&p[4],&p[5],&p[6],&p[7],&p[8],&p[9],
	&p[10],&p[11],&p[12],&p[13],&p[14],&p[15],&p[16],&p[17],&p[18],&p[19],
	&p[20],&p[21],&p[22],&p[23],&p[24],&p[25],&p[26],&p[27],&p[28],&p[29],
	&p[30],&p[31]) == FAILURE) 
	{
		RETURN_FALSE;
		return;
	}

	/* init */
	for (i=0;i<VIRT_CALLARG_MAX;i++) {
		argv[i] = (char *) NULL;
    desc[i].iType = 0; 
		desc[i].iLen = 0; 
		desc[i].iScale = 0;
    desc[i].iTypeIO = VIRT_IO_IN;
    desc[i].paseAddr = 0;
	}

	/* input */
	for (i=0;i < argc - 1;i++) {
		switch (Z_TYPE_P(p[i])) {
		case IS_NULL:
		case IS_LONG:
      desc[i].iType = VIRT_LONG; 
			desc[i].iLen = sizeof(i); 
      desc[i].iScale = 0;
      desc[i].iTypeIO = VIRT_IO_IN; 
      desc[i].paseAddr = 0;
			argv[i] = (char *) &((p[i])->value.lval);
			break;
		case IS_DOUBLE:
      desc[i].iType = VIRT_DOUBLE; 
			desc[i].iLen = sizeof(iamd); 
			desc[i].iScale = 0;
      desc[i].iTypeIO = VIRT_IO_IN; 
      desc[i].paseAddr = 0;
			argv[i] = (char *) &((p[i])->value.dval);
			break;
		case IS_STRING:
      desc[i].iType = VIRT_STRING; 
			desc[i].iLen = Z_STRLEN_P(p[i]); 
			desc[i].iScale = 0;
      desc[i].iTypeIO = VIRT_IO_IN; 
      desc[i].paseAddr = 0;
			argv[i] = (char *) Z_STRVAL_P(p[i]);
			break;
		case IS_ARRAY:
			RETURN_FALSE;
			return;
			break;
		case IS_OBJECT:
			RETURN_FALSE;
			return;
			break;
/*
		case IS_FALSE:
		case IS_TRUE:
		case IS_RESOURCE:
		case IS_REFERENCE:
		case IS_UNDEF:
*/
		default:
			RETURN_FALSE;
			return;
			break;
		}
	}

	rc = _ile_callback(name, &desc[0], argc - 1, argv);

	/* output */
	for (i=0;i < argc - 1;i++) {
		outval = PASE_ADDRESS64(desc[i].paseAddr);
		switch (Z_TYPE_P(p[i])) {
		case IS_NULL:
		case IS_LONG:
			switch(desc[i].iTypeIO) {
			case VIRT_IO_OUT:
			case VIRT_IO_BOTH:
      /* Z_LVAL_P(p[i]) = *(long *) outval; */
			break;
			}
			break;
		case IS_DOUBLE:
			switch(desc[i].iTypeIO) {
			case VIRT_IO_OUT:
			case VIRT_IO_BOTH:
      /* Z_DVAL_P(p[i]) = *(double *) outval; */
			break;
			}
			break;
		case IS_STRING:
			switch(desc[i].iTypeIO) {
			case VIRT_IO_OUT:
			case VIRT_IO_BOTH:
#if PHP_MAJOR_VERSION >= 7
        ZVAL_STR(p[i], zend_string_extend(Z_STR_P(p[i]), desc[i].iLen + 1, 0));
#else
				Z_STRVAL_P(p[i]) = estrndup(Z_STRVAL_P(p[i]), Z_STRLEN_P(p[i]));
				Z_STRVAL_P(p[i]) = erealloc(Z_STRVAL_P(p[i]), desc[i].iLen + 1);
				strcpy(Z_STRVAL_P(p[i]), outval);
				Z_STRLEN_P(p[i]) = desc[i].iLen;
#endif
				break;
			}
			break;
		case IS_ARRAY:
			RETURN_FALSE;
			return;
			break;
		case IS_OBJECT:
			RETURN_FALSE;
			return;
			break;
/*
		case IS_FALSE:
		case IS_TRUE:
		case IS_RESOURCE:
		case IS_REFERENCE:
		case IS_UNDEF:
*/
		default:
			RETURN_FALSE;
			return;
			break;
		}
		if (desc[i].paseAddr) {
			free(PASE_ADDRESS64(desc[i].paseAddr));
		}
	}

	RETURN_TRUE;
	return;
}
/* }}} */


/* {{{ 
*/
PHP_FUNCTION(ile_connect)
{
	char libmbr[32];
	memset(libmbr,0,sizeof(libmbr));
    strcpy(libmbr,ILE_G(i_lib));
    strcat(libmbr,"/VPHP");
	_ile_srvpgm(libmbr);
	_ile_pase("php_eval", (void *)_ile_call_script);
	_ile_connect(); /* return without exit */
}
/* }}} */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: sw=4 ts=4 fdm=marker
 * vim<600: sw=4 ts=4
 */
