      /if defined(VPHP_H)
      /eof
      /endif
      /define VPHP_H

       // *************************************************
       // PHP API -- start 32bit php
       // If PASE not running, start PHP 32bit using 
       // default script (chat.php). PHP script is expected 
       // to issue ile_connect(), releasing control to RPG.
       //   ini       (optional) - custom php.ini
       //   ileCCSID  (optional) - override ILE job CCSID
       //   paseCCSID (optional) - override ILE job CCSID
       // *************************************************
       dcl-pr PHPStart32 IND;
         ini CHAR(65500) VALUE options (*nopass);
         ileCCSID INT(10) VALUE options (*nopass);
         paseCCSID INT(10) VALUE options (*nopass);
       end-pr;

       // *************************************************
       // PHP API -- start 32bit php
       // If PASE not running, start PHP 32bit using 
       // script. PHP script is expected to issue ile_connect(), 
       // releasing control to RPG.
       //   script               - PHP /path/script
       //   ini       (optional) - custom php.ini
       //   ileCCSID  (optional) - override ILE job CCSID
       //   paseCCSID (optional) - override ILE job CCSID
       // *************************************************
       dcl-pr PHPStartMain32 IND;
         script CHAR(65500) VALUE;
         ini CHAR(65500) VALUE options (*nopass);
         ileCCSID INT(10) VALUE options (*nopass);
         paseCCSID INT(10) VALUE options (*nopass);
       end-pr;

       // *************************************************
       // PHP API -- call/pass php script
       // PHP eval script chunk.
       //   scriptChunk          - script chunk for PHP eval
       // *************************************************
       dcl-pr PHPScript32 IND;
         scriptChunk CHAR(65500) VALUE;
       end-pr;

       // *************************************************
       // PHP API -- stop php
       // Stop PHP.
       // *************************************************
       dcl-pr PHPStop;
       end-pr;


