     H NOMAIN
     H AlwNull(*UsrCtl)
     H BNDDIR('QC2LE')

      /copy iconf_h
      /copy vlic_h
      /copy vpase_h
      /copy vconv_h
      /copy virt_h
      /copy vphp_h

       // *************************************************
       // PHP API -- start 32bit php
       // *************************************************
       dcl-proc PHPStartMain32 export;
       dcl-pi *N IND;
        script CHAR(65500) VALUE;
        ini CHAR(65500) VALUE options (*nopass);
        ileCCSID INT(10) VALUE options (*nopass);
        paseCCSID INT(10) VALUE options (*nopass);
       end-pi;
       DCL-S rcb IND INZ(*OFF);
       dcl-s pgm CHAR(65500) inz(*BLANKS);
       dcl-s arg CHAR(65500) dim(8) inz(*BLANKS);
       dcl-s env CHAR(65500) dim(8) inz(*BLANKS);
       dcl-s myPaseCCSID INT(10) inz(819);

       pgm = PHP_PATH 
           + '/' + PHP_BIN;

       arg(1) = script;

       env(1) = 'PATH='
              + PHP_PATH 
              + ':' + PASE_PATH;
       env(2) = 'LIBPATH='
              + PHP_LIBPATH 
              + ':' + PASE_LIBPATH;
       env(3) = PASE_ATTACH;

       if %parms > 2;
         env(4) = 'PASE_CCSID=' + %char( paseCCSID );
         myPaseCCSID = paseCCSID;
       endif;
       if %parms > 1;
         setIleCCSID(ileCCSID);
       endif;
       if %parms > 0 and ini <> *BLANKS;
         arg(2) = '-n'; // remove zend defaults
         arg(3) = '-c';
         arg(4) = ini;
       endif;

       rcb = PaseStart32(pgm:arg:env:myPaseCCSID);

       // use default PASE copy in/out DftVirtualCall32,
       // but could override (little C++ think Mate)
       // VirtualSetLanguage(%paddr(PHPVirtualCall32));

       return rcb;
       end-proc; 


       // *************************************************
       // PHP API -- start 32bit php
       // *************************************************
       dcl-proc PHPStart32 export;
       dcl-pi *N IND;
        ini CHAR(65500) VALUE options (*nopass);
        ileCCSID INT(10) VALUE options (*nopass);
        paseCCSID INT(10) VALUE options (*nopass);
       end-pi;
       DCL-S rcb IND INZ(*OFF);
       dcl-s script CHAR(65500) inz(*BLANKS);

       script = PHP_SHARE 
              + '/' + PHP_VLANG 
              + '/' + PHP_CHAT;

       if %parms > 2;
         rcb = PHPStartMain32(script:ini:ileCCSID:paseCCSID);
       elseif %parms > 1;
         rcb = PHPStartMain32(script:ini:ileCCSID);
       elseif %parms > 0;
         rcb = PHPStartMain32(script:ini);
       else;
         rcb = PHPStartMain32(script);
       endif;

       return rcb;
       end-proc; 

       // *************************************************
       // PHP API -- call/pass php script
       // *************************************************
       dcl-proc PHPScript32 export;
       dcl-pi *N IND;
        scriptChunk CHAR(65500) VALUE;
       end-pi;
       DCL-S rcb IND INZ(*OFF);

       rcb = VirtualCallScript32('php_eval':scriptChunk);

       return rcb;
       end-proc; 

       // *************************************************
       // PHP API -- stop php script
       // *************************************************
       dcl-proc PHPStop export;
         PaseStop();
       end-proc;

