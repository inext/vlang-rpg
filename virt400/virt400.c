#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <limits.h>
#include "virt400.h"

#if ( __WORDSIZE == 64 )
#define PASE_TO_ADDRESS64(x) ((address64_t)((uint64)x))
#else
#define PASE_TO_ADDRESS64(x) ((address64_t)((uint32)x))
#endif

static char virtSrvPgm[1024];

int vlangActMark;
static int _VirtualLang() {
	int rc = 0;
	if (!vlangActMark) {
		vlangActMark = _ILELOAD(virtSrvPgm, ILELOAD_LIBOBJ);
	}
	if (vlangActMark < 1) {
#ifdef PASE_DEBUG
		printf("_VirtualLang ileload failed %s\n", virtSrvPgm);
#endif
		return -1;
	}
	return 0;
}

typedef struct VirtualInitStruct {ILEarglist_base base; ILEpointer n; ILEpointer p; } VirtualInitStruct;
static char VirtualInitBuf[256];
static char * VirtualInitPtr;
static int _VirtualInit(char *name, void *func) {
	int rc = 0;
	VirtualInitStruct * arglist = (VirtualInitStruct *) NULL;
	char buffer[ sizeof(VirtualInitStruct) + 16 ];
	static arg_type_t VirtualInitSigStruct[] = { ARG_MEMPTR, ARG_MEMPTR, ARG_END };
	rc = _VirtualLang();
	if (rc < 0) {
		return 0;
	}
	if (!VirtualInitPtr) {
		VirtualInitPtr = (char *)ROUND_QUAD(VirtualInitBuf);
		rc = _ILESYM((ILEpointer *)VirtualInitPtr, vlangActMark, "VIRTUALINIT");
		if (rc < 0) {
#ifdef PASE_DEBUG
			printf("_VirtualInit ilesym failed VIRTUALINIT\n");
#endif
			return 0;
		}
	}
	memset(buffer,0,sizeof(buffer));
	arglist = (VirtualInitStruct *)ROUND_QUAD(buffer);
	arglist->n.s.addr = PASE_TO_ADDRESS64(name);
	arglist->p.s.addr = PASE_TO_ADDRESS64(func);
	rc = _ILECALL((ILEpointer *)VirtualInitPtr, &arglist->base, VirtualInitSigStruct, RESULT_INT32);
	if (rc != ILECALL_NOERROR) {
#ifdef PASE_DEBUG
		printf("_VirtualInit ilecall failed ILECALL_NOERROR != %d\n", rc);
#endif
		return 0;
	}
	return 1;
}

typedef struct VirtualCallBack32Struct {ILEarglist_base base; ILEpointer n; ILEpointer d; ILEpointer p[VIRT_CALLARG_MAX]; } VirtualCallBack32Struct;
static char VirtualCallBack32Buf[256];
static char * VirtualCallBack32Ptr;
static int _VirtualCallBack32(char * name, virtVar_t *desc, int argc, char *argv[]) {
	int rc = 0, i = 0, idx = -1;
	VirtualCallBack32Struct * arglist = (VirtualCallBack32Struct *) NULL;
	char buffer[ sizeof(VirtualCallBack32Struct) + 16 ];
	static arg_type_t VirtualCallBack32SigStruct[] = { ARG_MEMPTR, ARG_MEMPTR,
	ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,
	ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,
	ARG_MEMPTR,ARG_MEMPTR,
	ARG_END 
	};
	rc = _VirtualLang();
	if (rc < 0) {
		return 0;
	}
	if (!VirtualCallBack32Ptr) {
		VirtualCallBack32Ptr = (char *)ROUND_QUAD(VirtualCallBack32Buf);
		rc = _ILESYM((ILEpointer *)VirtualCallBack32Ptr, vlangActMark, "VIRTUALCALLBACK32");
		if (rc < 0) {
			return 0;
		}
	}
	memset(buffer,0,sizeof(buffer));
	arglist = (VirtualCallBack32Struct *)ROUND_QUAD(buffer);
	arglist->n.s.addr = PASE_TO_ADDRESS64(name);
	arglist->d.s.addr = PASE_TO_ADDRESS64(desc);
	for (i=0;i<argc;i++) {
		arglist->p[i].s.addr = PASE_TO_ADDRESS64(argv[i]);
	}
	rc = _ILECALL((ILEpointer *)VirtualCallBack32Ptr, &arglist->base, VirtualCallBack32SigStruct, RESULT_INT32);
	if (rc != ILECALL_NOERROR) {
		return 0;
	}
	return 1;
}

typedef struct VirtualCallBackArgv32Struct {ILEarglist_base base; ILEpointer n; ILEpointer p[VIRT_CALLARG_MAX]; } VirtualCallBackArgv32Struct;
static char VirtualCallBackArgv32Buf[256];
static char * VirtualCallBackArgv32Ptr;
static int _VirtualCallBackArgv32(char * name, int argc, char *argv[]) {
	int rc = 0, i = 0, idx = -1;
	VirtualCallBackArgv32Struct * arglist = (VirtualCallBackArgv32Struct *) NULL;
	char buffer[ sizeof(VirtualCallBackArgv32Struct) + 16 ];
	static arg_type_t VirtualCallBackArgv32SigStruct[] = { ARG_MEMPTR,
	ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,
	ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,ARG_MEMPTR,
	ARG_MEMPTR,ARG_MEMPTR,
	ARG_END 
	};
	rc = _VirtualLang();
	if (rc < 0) {
		return 0;
	}
	if (!VirtualCallBackArgv32Ptr) {
		VirtualCallBackArgv32Ptr = (char *)ROUND_QUAD(VirtualCallBackArgv32Buf);
		rc = _ILESYM((ILEpointer *)VirtualCallBackArgv32Ptr, vlangActMark, "VIRTUALCALLBACKARGV32");
		if (rc < 0) {
			return 0;
		}
	}
	memset(buffer,0,sizeof(buffer));
	arglist = (VirtualCallBackArgv32Struct *)ROUND_QUAD(buffer);
	arglist->n.s.addr = PASE_TO_ADDRESS64(name);
	for (i=0;i<argc;i++) {
		arglist->p[i].s.addr = PASE_TO_ADDRESS64(argv[i]);
	}
	rc = _ILECALL((ILEpointer *)VirtualCallBackArgv32Ptr, &arglist->base, VirtualCallBackArgv32SigStruct, RESULT_INT32);
	if (rc != ILECALL_NOERROR) {
		return 0;
	}
	return 1;
}

int _ile_callback_argv(char * name, int argc, char *argv[])
{
	int rc = 0;
	rc = _VirtualCallBackArgv32(name, argc, argv);
	return rc;
}

int _ile_callback(char * name, virtVar_t *desc, int argc, char *argv[])
{
	int rc = 0;
	rc = _VirtualCallBack32(name, desc, argc, argv);
	return rc;
}

int _ile_srvpgm(char * srvpgm) {
	int rc = 0;
	strcpy(virtSrvPgm, srvpgm);
	return rc;
}

int _ile_pase(char *name, void *func) {
	int rc = 0;
	rc = _VirtualInit(name, func);
	return rc;
}

void _ile_connect() {
	_RETURN();
}


