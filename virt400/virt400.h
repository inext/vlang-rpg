#ifndef _VIRT400_H
#define _VIRT400_H
#include <as400_protos.h>
#include <as400_types.h>

/*
 * match virt_h.rpgle
 */
#define VIRT_TABLE_MAX 4096
#define VIRT_CALLARG_MAX 32

#define VIRT_CHAR 1
#define VIRT_VARCHAR 2
#define VIRT_USC2 3
#define VIRT_VARUSC2 4
#define VIRT_GRAPH 5
#define VIRT_VARGRAPH 6
#define VIRT_IND 7
#define VIRT_PACKED 8
#define VIRT_ZONED 9
#define VIRT_BINDEC 10
#define VIRT_INT3 11
#define VIRT_INT5 12
#define VIRT_INT10 13
#define VIRT_LONG 13
#define VIRT_IN20 14
#define VIRT_UNS3 15
#define VIRT_UNS5 16
#define VIRT_UNS10 17
#define VIRT_UNS20 18
#define VIRT_FLOAT4 19
#define VIRT_FLOAT8 20
#define VIRT_DOUBLE 20
#define VIRT_DATE 21
#define VIRT_TIME 22
#define VIRT_TIMESTAMP 23
#define VIRT_POINTER 24
#define VIRT_POINTERPROC 25
#define VIRT_STRING 26

#define VIRT_IO_IN 1
#define VIRT_IO_OUT 2
#define VIRT_IO_BOTH 3

#define ROUND_QUAD(x) (((size_t)(x) + 0xf) & ~0xf)

typedef struct virtVar_t {int iType; int iLen; int iScale; int iTypeIO; address64_t paseAddr; } virtVar_t;
int _ile_callback(char * name, virtVar_t *desc, int argc, char *argv[]);
int _ile_callback_argv(char * name, int argc, char *argv[]);
int _ile_srvpgm(char * srvpgm);
int _ile_pase(char *name, void *func);
void _ile_connect();



#endif /* _VIRT400_H */
