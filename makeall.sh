#!/QOpenSys/usr/bin/ksh

echo "====================================================="
echo "====================================================="
if [ ! -d /QSYS.LIB ]; then
  if [ "x$CHROOT" = "x" ]; then 
    echo "env var CHROOT not set  (export CHROOT=/QOpenSys/zend7)"
    exit
  fi
fi
echo "== CHROOT ($CHROOT) =="

if [ "x$PHP_HOME" = "x" ]; then
    echo "PHP_HOME not set  (export PHP_HOME=/usr/local/zendphp7)"
    exit
fi
if [ ! -d $PHP_HOME ]; then
    echo "PHP_HOME $PHP_HOME not found"
    exit
fi
echo "== PHP ($PHP_HOME) =="

if [ "x$ILELIB" = "x" ]; then 
    echo "ILELIB not set  (export ILELIB=VLANG)"
    exit
fi
echo "== ILELIB ($ILELIB) =="


if [ ! -f /QOpenSys/usr/bin/crtrpgmod ]; then
    echo "borgi utility /QOpenSys/usr/bin/crtrpgmod not found"
    echo "project borgi required for ILE Makefile compiles"
    echo "( download src https://bitbucket.org/litmis/borgi )"
    echo "( pre-compiled http://yips.idevcloud.com/wiki/index.php/Databases/Borgi )"
    exit
fi
echo "== project borgi ok /QOpenSys/usr/bin/crtrpgmod (https://bitbucket.org/litmis/borgi) =="

export IBM_DB_HOME=/usr
export PASE_TOOLS_HOME=/QOpenSys/usr
export AIX_TOOLS_HOME=/usr/local
export PATH=$PHP_HOME/bin:$PASE_TOOLS_HOME/bin:$AIX_TOOLS_HOME/bin:$PATH
export LIBPATH=$PHP_HOME/lib:$PASE_TOOLS_HOME/lib:$AIX_TOOLS_HOME/lib
export CC=gcc
export CFLAGS="-gxcoff -DPASE -I=.:$PHP_HOME/php/include"
export CCHOST=powerpc-ibm-aix6.1.0.0
export LANG=C


echo "====================================================="
echo "====================================================="
if [ "x$CHROOT" = "x" ]; then 
  echo "== installed root =="
else
  PWD=$(pwd)
  CPWD="$CHROOT$PWD"
  echo "== cpy2root.sh  ==="
  echo "== $CPWD copy files to $PHP_HOME ===" 
  echo "#!/QOpenSys/usr/bin/ksh" > cpy2root.sh
  echo "mkdir -p $PHP_HOME/share/vlang/php" >> cpy2root.sh
  echo "cp $CPWD/virt400/libvirt400.a /QOpenSys/usr/lib/." >> cpy2root.sh
  echo "cp $CPWD/pecl_ile/ile.so $PHP_HOME/lib/php_extensions/." >> cpy2root.sh
  echo "cp $CPWD/pecl_ile/ile.ini $PHP_HOME/etc/conf.d/." >> cpy2root.sh
  echo "cp $CPWD/tests/php/* $PHP_HOME/share/vlang/php/." >> cpy2root.sh
  echo "cp $CPWD/tests/c/*32 $PHP_HOME/share/vlang/php/." >> cpy2root.sh
  chmod +x cpy2root.sh
fi


echo "====================================================="
echo "====================================================="
echo "== pase libvirt400.a ==="
cd virt400
make tgt32 tgt64 install
cd ..

echo "====================================================="
echo "====================================================="
echo "== pecl_ile ile.so ==="
cd pecl_ile
if [ ! -f Makefile ]; then
  phpize
  ./configure --build=$CCHOST --host=$CCHOST --target=$CCHOST
fi
make
make install
echo "== pecl_ile copy files to $PHP_HOME ==="
cp ile.so $PHP_HOME/lib/php_extensions/.
cp ile.ini $PHP_HOME/etc/conf.d/.
mkdir -p $PHP_HOME/share/vlang/php
cp ../tests/php/chat.php $PHP_HOME/share/vlang/php/.
cd ..

echo "====================================================="
echo "====================================================="
echo "== vphp  $ILELIB/vphp.srvpgm ==="
cd vphp
make vphp
cd ..

echo "====================================================="
echo "====================================================="
echo "== tests/c  ==="
cd tests/c
make tgt32 install
cd ../..

echo "====================================================="
echo "====================================================="
echo "== tests/rpg  ==="
cd tests/rpg
make rpg
cd ../..



