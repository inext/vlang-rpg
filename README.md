# README #

### RPG control scripting language (PHP) ###
This project attempts to answer the old question of calling between RPG<>PASE in a single job. 
The focus is for scripting languages run by RPG. This project is not like PHP toolkit, 
ruby toolkit, python toolkit, etc. Instead, this project is RPG-centric, 
aka, RPG in charge with scripting language as slave, running in the same process. 
The project intends to advance the state of the art beyond anything today, actually, read/write, 
call/return between RPG and scripting language.

The following information references PHP as example "scripting language" (README_PHP.md). 
Nothing special about PHP, fine scripting language, but VLANG call/callback techniques apply to 
any language with 'eval script' capabilities (soon).

VLANG is a RPG SRVPGM (VLANG/VPHP), providing in-memory calling of RPG<memory>PHP,
or any other scripting language implementing an ile callback extension. 
Everything is controlled by RPG, thereby, PHP, or any PASE language, 
is essentially a slave language to RPG.

VLANG is RPG-centric, aka, RPG starts PHP, or any language as a slave language,
where, libvirt400.a function _ile_connect exists the language back to
RPG VLANG control (your RPG program). Therefore, you may not use pecl ile.so extension 
starting from PHP, only start from RPG. However, you may use php or other language
from your RPG CGI (see testjson.md).

Note: Compile alternative library is enabled. Alternative pre-compiled test binaries
are available Yips (see below). Therefore VLANG/PHP documentation 
substitute version matching php installation. Please install correct version (pase<>ile).

* VLANG7/VPHP - Zend Server 9 (php 7.1 - /usr/local/zendphp7)
* VLANG5/VPHP - Zend Server 8 (php 5.6 - /usr/local/zendsvr6)

### RPG control PASE ###
VLANG can be use without a scripting language (README_PASE.md), allowing calls to
most anything PASE. The trick is to create a PASE main using libvirt400.a 
function _ile_connect, to return PASE control to your RPG program without
actually exiting PASE (testpase). You may add many other shared libraries, calling
function in normal PASE c API, then provide a callback mapping for your 
RPG program (testssl). 

VLANG 'PASE mode' also allows bypass of all SRVPGM conversions, passing raw argv style
arguments (README_PASE.md). Hard work for RPG programmer to be sure, many conversions. 
If you must chase every performance penny, raw argv is likely highest performing 
option (testdiy).

### programs ###

```
vlang/vphp - *SRVPGM ILE/RPG interface to RPG virtual language (virtual scripting)
ile.so - PHP extension to pass control to RPG, eval script chunks, and callback into RPG 
libvirt400.a - PASE virtual callback driver (new driver)
```

Note: libvirt400.a is new, enables a PASE API abstraction to virtual start/callback.
Enables other scripting languages to use same support without re-writing 'virtual' code 
(python, nodejs, ruby, etc.). Sets framework to enable RPG to load/call PASE anything (thing.so, libthing.a), 
at 'high speed' with less code effort (testpase.md). Tests use PHP directories, but,
for pase machine-wide access, you may install into directory /QOpenSys/usr/lib/libvirt400.a.

### compile source (vlang/vphp) ###
download zip (Downloads left panel)
```
=== zend server 9 (php7.1) ===
> export PATH=/usr/local/zendphp7/bin:/QOpenSys/usr/bin
> export LIBPATH=/usr/local/zendphp7/lib:/QOpenSys/usr/lib
> export CHROOT=/QOpenSys/zend7
> export ILELIB=VLANG7
> export PHP_HOME=/usr/local/zendphp7
> ./makeall.sh
> ./makeclean.sh

=== zend server 8 (php 5.6) ===
> export PATH=/usr/local/zendsvr6/bin:/QOpenSys/usr/bin
> export LIBPATH=/usr/local/zendsvr6/lib:/QOpenSys/usr/lib
> export CHROOT=/QOpenSys/zend5
> export ILELIB=VLANG5
> export PHP_HOME=/usr/local/zendsvr6
> ./makeall.sh
> ./makeclean.sh

```
Notes: 
Requires borgi project (https://bitbucket.org/litmis/borgi). Requires GCC set up.

### Alternative pre-compiled

* http://yips.idevcloud.com/wiki/index.php/RPG/RPGCallMe
* php7 and php5 hard coded LIB and PHP path, install correct version (pase<>ile)
```
=== vitual ILE callback enable ===
/QOpenSys/usr/lib
1) vlang-rpg-n.n.n/virt400/libvirt400.a to ... /QOpenSys/usr/lib/


=== zend server 9 (php7.1) ===
PHP_HOME=/usr/local/zendphp7
1) vlang-rpg-n.n.n/php7/vlang7.savf to ... VLANG7/VPHP
2) vlang-rpg-n.n.n/php7/pecl_ile/ile.so to ... $PHP_HOME/lib/php_extensions/
3) vlang-rpg-n.n.n/php7/pecl_ile/ile.ini to ... $PHP_HOME/etc/conf.d/
4) vlang-rpg-n.n.n/php7/tests/php to ... $PHP_HOME/share/vlang/php/
5) vlang-rpg-n.n.n/php7/tests/c to ... $PHP_HOME/share/vlang/php/
6) check correct ile.ini settings (VLANG7)
> cat /usr/local/zendphp7/etc/conf.d/ile.ini
extension=ile.so
ile.i_lib="VLANG7"


=== zend server 8 (php 5.6) ===
PHP_HOME=/usr/local/zendsvr6
1) vlang-rpg-n.n.n/php5/vlang5.savf to ... VLANG5/VPHP
2) vlang-rpg-n.n.n/php5/pecl_ile/ile.so to ... $PHP_HOME/lib/php_extensions/
3) vlang-rpg-n.n.n/php5/pecl_ile/ile.ini to ... $PHP_HOME/etc/conf.d/
4) vlang-rpg-n.n.n/php5/tests/php to ... $PHP_HOME/share/vlang/php/
5) vlang-rpg-n.n.n/php5/tests/c to ... $PHP_HOME/share/vlang/php/
6) check correct ile.ini settings (VLANG5)
>cat /usr/local/zendsvr6/etc/conf.d/ile.ini 
extension=ile.so
ile.i_lib="VLANG5"


Note: 
- chat.php required at $PHP_HOME/share/vlang/php/chat.php
```

### examples ###
You can find PHP examples Source link (rpg/php)

* test0001.rpgle / helloile.php - exchange in/out params (see test0001.md)
* testmysql.rpgle / mysqlanimal.php - RPG receive mysql fetch records (see testmysql.md)
* testyahoo.rpgle / yahoofood.php - RPG receive yahoo search food (see testyahoo.md)
* testjson.rpgle / jsonile.php - RPG CGI using PHP json_decode (see testjson.md)
* testwatson.rpgle / watson_trans.php - RPG CGI using PHP Watson language-translator (see testwatson.md)

You can find PASE examples Source link (rpg/pase - no PHP)

* testpase.rpgle / testhello.c - exchange in/out params with conversion (see testpase.md)
* testdiy.rpgle / testargv.c - exchange in/out params, high speed do it yourself, no auto-conversion (see testdiy.md)
* testssl.rpgle / testsslbasic.c - RPG use PASE OpenSSL APIs for secure request (see testssl.md)

### example run (see rpg/php source) ###
```
ssh me@myibmi
$ system "call vlang7/testyahoo"
-- or --
$ system "call vlang5/testyahoo"

Beijing Express -- 3434 55th St Nw, #150
Wong's Restaurant -- 1654 Highway 52 N
China Star -- 2808 41st St Nw
Great Wall -- 3110 Wellner Dr Ne
China King -- 3434 55th St Nw
China One -- 1117 Civic Center Dr Nw
China Star -- 1505 Highway 14 E
Waiter Express Food Deliv -- 829 3rd Ave Se
Hunan Garden Chinese Rest -- 1120 7th St Nw
China Star -- 405 1st Ave Sw
bash-4.3$ 

```

### Performance ###
Do not use a ILE/PASE debugger while running performance tests. We have learned by painful experience, 
debuggers greatly extend PASE start time. Dramatic increase in time debugger 
PHPStart32 greater minute vs. non-debug PHPStart32 in seconds. 

### debug ###
* RPG can be debugged using standard ILE debuggers (many).
* PHP c code can be debugged by putting sleep in the PHP code, then attaching a PASE debugger (dbx -d 100 -a pid).
* PHP scripts, i dunno, never used a PHP debugger (see Alan, maybe he knows)

### Contribution guidelines ###
* [Watch this](http://bit.ly/ibmi-git-pull-request) to learn how to do pull requests on IBM i.
* Everything you contribute is under BSD license, aka, you are giving to community.
* All RPG you create should be RPG free form

### RPG free form (join the future, yes??) ###
* http://www.ibm.com/developerworks/ibmi/library/i-ibmi-rpg-support/

### PHP ###
* https://github.com/php/php-src/blob/master/Zend/zend_types.h
* https://github.com/php/php-src/blob/master/Zend/zend_execute_API.c (see zend_eval_string)

### Whom do I talk to? ###
* adc@us.ibm.com

### GCC notes
```
===
IMPORTANT: as400_types.h must have force gcc align quadword (see edit above) 
===
> grep gcc /usr/include/as400*       
/usr/include/as400_types.h:    long double      align __attribute__((aligned(16))); /* force gcc align quadword */

=== edit this file ===
/usr/include/as400_types.h:
/*
 * Quadword (aligned) area for a tagged ILE pointer
 */
typedef union _ILEpointer {
#if !(defined(_AIX) || defined(KERNEL))
#pragma pack(1,16,_ILEpointer)	/* Force sCC quadword alignment */
#endif
/* CAUTION: Some compilers only provide 64-bits for long double */
/*========================GCC CHANGE =================================*/
#if defined( __GNUC__ )
    long double	align __attribute__((aligned(16))); /* force gcc align quadword */
#else
    long double align;	/* Force xlc quadword alignment (with -qldbl128 -qalign=natural) */
#endif
/*========================GCC CHANGE =================================*/
#ifndef _AIX
    void		*openPtr; /* MI open pointer (tagged quadword) */
#endif
    struct {
	char		filler[16-sizeof(uint64)];
	address64_t	addr;	/* (PASE) memory address */
    } s;
} ILEpointer;
```

