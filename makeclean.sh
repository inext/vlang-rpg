#!/QOpenSys/usr/bin/ksh

if [ ! -d /QSYS.LIB ]; then
  if [ "x$CHROOT" = "x" ]; then 
    echo "env var CHROOT not set"
    exit
  fi
fi
echo "== CHROOT ($CHROOT) =="

if [ "x$ILELIB" = "x" ]; then 
  ILELIB="VLANG"
fi
echo "== ILELIB ($ILELIB) =="

echo "=== clean up $ILELIB/*MODULE ==="
dltmod --lib $ILELIB --mod VPHP
dltmod --lib $ILELIB --mod VCONV
dltmod --lib $ILELIB --mod VIRT
dltmod --lib $ILELIB --mod VPASE
dltmod --lib $ILELIB --mod TEST0001
dltmod --lib $ILELIB --mod TESTDIY
dltmod --lib $ILELIB --mod TESTJSON
dltmod --lib $ILELIB --mod TESTMYSQL
dltmod --lib $ILELIB --mod TESTPASE
dltmod --lib $ILELIB --mod TESTSSL
dltmod --lib $ILELIB --mod TESTYAHOO
dltmod --lib $ILELIB --mod TESTWATSON

echo "=== create savf (QGPL/$ILELIB)==="
borgi -c "crtSAVF FILE(QGPL/$ILELIB)"
borgi -c "clrSAVF FILE(QGPL/$ILELIB)"
borgi -c "SAVLIB LIB($ILELIB) DEV(*SAVF) SAVF(QGPL/$ILELIB)"

