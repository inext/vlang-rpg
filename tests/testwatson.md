# testwatson.rpgle #
Sample testwatson is a RPG program calling PHP to access Watson REST API. 
In this test, accessing 'free' language-translator/api/v2/translate. 

```
RPG (call):
       enChar100 = 'This is a test';
       pgm = 
             '$english="'+enChar100+'";'
           + '$model="en-ja";'
           + 'include "'
           + PHP_SHARE 
           + '/' + PHP_VLANG 
           + '/' + 'watson_trans.php";';
       PHPScript32(pgm);

PHP (watson_trans.php):
$url="https://watson-api-explorer.mybluemix.net/language-translator/api/v2/translate";
$data_string="model_id=$model&text=$english";
$context  = stream_context_create(
  array('http' =>
    array(
      'method'  => 'POST',
      'header'  => "Content-type: application/x-www-form-urlencoded\r\n"
                 . 'Content-Length: ' . strlen($data_string) . "\r\n"
                 . 'accept: text/plain'."\r\n",
      'content' => $data_string
    )
  )
);
$meChar100 = file_get_contents($url, false, $context);
ile_callback("IleJaMe",$meChar100);


RPG (callback):
       dcl-proc IleJaMe export;
       dcl-pi *N;
        meChar100 CHAR(100);
       end-pi;
```


### Source code ###
```
rpg/testwatson.rpgle
php/watson_trans.php
```

### Run ###
```
call qp2term
> system "call vlang/testwatson"
```

### PHP ###

I am using ADC *SECOFR because not all IBM i profiles are authorized to use php, therefore testwatson fails. 
This is not a recommended way to enable RPG usage of php, you will need traditional security administrative action on php directories to enable your profile (i was lazy).



### Debug ###
Always check php.log when things go wrong, saves many hours of stress.

```
bash-4.3$ tail var/log/php.log 
[05-Apr-2016 14:28:58 America/Atikokan] PHP Warning:  ile_connect():
```

