# testyahoo.rpgle #
Sample testyahoo is a RPG program calling PHP to access a REST published database. In this test, accessing 'free' Yahoo restaurant locations by desired food type ($food) in a zip code area ($zip). 

```
RPG (set php variables, include script):
       food_script =
         '$zip="55906";'
       + '$food="chinese";'
       + 'include "share/vlang/php/yahoofood.php";';
       PHPScript32(food_script);

PHP (yahoofood.php):
$yql_base_url = "http://query.yahooapis.com/v1/public/yql";
$yql_query = "select * from local.search where zip='{$zip}' and query='{$food}'";
:
if(!is_null($phpObj->query->results)){  
  foreach($phpObj->query->results->Result as $place){
    ile_callback("YahooFood", $place->Title, $place->Address);  
  }  
}


RPG (callback):
       dcl-proc YahooFood export;
         dcl-pi *N;
          title CHAR(25);
          addr CHAR(25);
         end-pi;
```

This restaurant database is real, therefore, the test is also real data. In fact, testyahoo works great in my home town town to find places to eat.

### Source code ###
```
rpg/testyahoo.rpgle
php/yahoofood.php
```

### Run ###
```
call qp2term
> cd  share/vlang
> system "call vlang/testyahoo"
```

### PHP ###

I am using ADC *SECOFR because not all IBM i profiles are authorized to use php, therefore testyahoo fails. This is not a recommended way to enable RPG usage of php, you will need traditional security administrative action on php directories to enable your profile (i was lazy).

### Debug ###
Always check php.log when things go wrong, saves many hours of stress.

```
bash-4.3$ tail var/log/php.log 
[05-Apr-2016 14:28:58 America/Atikokan] PHP Warning:  ile_connect():
```

