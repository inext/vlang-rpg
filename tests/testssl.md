# testssl.rpgle #

Sample testssl is a RPG program calling PASE
to perform secure request using PASE OpenSSL . 
This test uses the SRVPGM built in conversion callback via 'descriptions'.
Note: This test does NOT start PHP, but use VLANG/VPHP.
I should create another *SRVPGM (VLANG/VPASE), but i was lazy,
and, srvpgm name really does not matter, PHP is not started.

Article link (code copy)

* http://www.ibm.com/developerworks/library/l-openssl/

Cert from following site.

```
wget https://curl.haxx.se/ca/cacert.pem
```


```
testsslbasic.c:

int _ile_respond(char * data) {
:
	rc = _ile_callback(name, &desc[0], argc, argv);
:
}
int _ile_request(char * request) {
    BIO * bio;
    SSL * ssl;
    SSL_CTX * ctx;
    char r[1024];
:
    /* Read in the response */

    for(;;)
    {
        p = BIO_read(bio, r, 1023);
        if(p <= 0) break;
        r[p] = 0;
        _ile_respond(r);
    }
:
}
int main(int argc, char *argv[]) {
	_ile_srvpgm("VLANG/VPHP");
    _ile_pase("hey_ssl", (void *)_ile_request);
	_ile_connect(); /* return without exit */
}

testssl.rpgle:

       VirtualAddCallBack('IleSSL':%paddr(IleSSL));
       VirtualAddCallBackDesc('IleSSL':VIRT_CHAR:1024:0:VIRT_IO_IN);
       VirtualCompleteCallBack('IleSSL');

       request = 'GET / HTTP/1.1\nHost: www.verisign.com'
               + CRLF
               + 'Host: www.verisign.com'
               + CRLF
               + 'Connection: Close'
               + CRLF
               + CRLF;
       rcb = VirtualCallScript32('hey_ssl':request);


       // *************************************************
       // callback location
       // *************************************************
       dcl-proc IleSSL export;
         dcl-pi *N;
          meChar CHAR(1024);
         end-pi;

         response = %trim(response) + meChar;

       end-proc; 
```

### Source code ###
```
rpg/testssl.rpgle
php/testsslbasic.c
```

### Run ###
```
call qp2term
> cd  share/vlang
> system "call vlang/testssl"
```

