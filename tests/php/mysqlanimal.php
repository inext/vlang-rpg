<?php
/*
bash-4.3$ export PATH=.:/usr/local/mysql/bin:/QOpenSys/usr/bin:/usr/ccs/bin:/QOpenSys/usr/bin/X11:/usr/sbin:/usr/bin
bash-4.3$ export LIBPATH=.:/usr/local/mysql/lib:/QOpenSys/usr/lib:/usr/lib
bash-4.3$ mysql --user root mysql
mysql> create database ranger;
mysql> CREATE USER 'ranger'@'localhost' IDENTIFIED BY 'ranger';
mysql> GRANT ALL PRIVILEGES ON * . * TO 'ranger'@'localhost';
mysql> FLUSH PRIVILEGES;
mysql> select host, user, password from mysql.user;
mysql> exit
bash-4.3$mysql -u ranger -p
mysql> exit
*/
$link = mysql_connect('localhost', 'ranger', 'ranger');

mysql_select_db("ranger", $link);
//echo $sql . " " . mysql_errno($link) . ": " . mysql_error($link). "\n";

//Drop the animal table, in case it exists
$sql = 'drop table animals';
mysql_query($sql, $link);
//echo $sql . " " . mysql_errno($link) . ": " . mysql_error($link). "\n";

//Create the animal table
$sql = 'create table animals (id integer UNSIGNED AUTO_INCREMENT PRIMARY KEY, breed varchar(32), name char(16), weight decimal(7,2))';
mysql_query($sql, $link);
//echo $sql . " " . mysql_errno($link) . ": " . mysql_error($link). "\n";

//Populate the animal table
$animals = array(
  array(0, 'cat',        'Pook',         3.2),
  array(1, 'dog',        'Peaches',      12.3),
  array(2, 'horse',      'Smarty',       350.0),
  array(3, 'gold fish',  'Bubbles',      0.1),
  array(4, 'budgerigar', 'Gizmo',        0.2),
  array(5, 'goat',       'Rickety Ride', 9.7),
  array(6, 'llama',      'Sweater',      150)
);
foreach($animals as $row){
  $sql = "insert into animals (id, breed, name, weight) VALUES ({$row[0]},'{$row[1]}','{$row[2]}',{$row[3]})";
  mysql_query($sql, $link);
  //echo $sql . " " . mysql_errno($link) . ": " . mysql_error($link). "\n";
}
if (!isset($limit)) $limit=count($animals);
$sql = "select id, breed, name, weight from animals limit {$limit}";
$result = mysql_query($sql, $link);
while ($row = mysql_fetch_array($result)) {
 // echo "{$row['id']}, {$row['breed']}, {$row['name']}, {$row['weight']}\n";
 ile_callback("MySqlAnimal", $row['id'], $row['breed'], $row['name'], $row['weight']);  
}

?>
