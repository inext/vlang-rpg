<?php
error_log("watson_trans.php we are about to start ...", 0);
$ile = extension_loaded ('ile');
if (!$ile) {
  echo "ile extension not loaded.\n";
  $english="This is a test";
  $model="en-ja";
}
error_log("watson_trans.php we are about to callback to ILE IleJaMe  ...", 0);
/*
 * plain text style
 */
$url="https://watson-api-explorer.mybluemix.net/language-translator/api/v2/translate";
$data_string="model_id=$model&text=$english";
$context  = stream_context_create(
  array('http' =>
    array(
      'method'  => 'POST',
      'header'  => "Content-type: application/x-www-form-urlencoded\r\n"
                 . 'Content-Length: ' . strlen($data_string) . "\r\n"
                 . 'accept: text/plain'."\r\n",
      'content' => $data_string
    )
  )
);
$meChar100 = file_get_contents($url, false, $context);
echo "before ILE call text ... $english\n";
echo "before ILE call tmodel ... $model\n";
echo "watson ... $meChar100\n";
ile_callback("IleJaMe",$meChar100);
error_log("watson_trans.php we are back from ILE IleJaMe.", 0);

/*
 *
 * ==== how to fix SSL error ===
 *
 * error:14090086:SSL routines:ssl3_get_server_certificate
 *
 * > $ php-cli -r 'var_dump(openssl_get_cert_locations());'
 * array(8) {
 * ["default_cert_file"]=>
 * string(38) "/usr/local/openssl-1.0.2j/ssl/cert.pem"
 * :
 *
 * > mkdir -p /usr/local/openssl-1.0.2j/ssl/
 *
 * > php-cli -r 'ini_set("display_errors", 1);
 *   $context = stream_context_create(["ssl" =>["verify_peer" => false]]);
 *   readfile("https://curl.haxx.se/ca/cacert.pem", false, $context);' > /usr/local/openssl-1.0.2j/ssl/cert.pem
 */

?>
