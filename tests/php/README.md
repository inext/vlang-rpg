# README_PHP #

VLANG is a RPG SRVPGM (VLANG/VPHP), providing in-memory calling of RPG<memory>PHP. 
Everything is controlled by RPG, thereby, PHP is essentially a slave language to RPG.

Note: VLANG is RPG-centric, aka, RPG starts PHP as a slave language. 
Therefore, you may not use pecl ile.so extension starting from PHP.
However, you may use php from your RPG CGI (see testjson.md).

### How do I get set up? ###
See main README.md.

### Performance ###
Do not use a ILE/PASE debugger while running performance tests. 
We have learned by painful experience, 
debuggers greatly extend PASE start time. Dramatic increase in time debugger 
PHPStart32 greater minute vs. non-debug PHPStart32 in seconds. 

PHP starts faster with custom php.ini with fewer extensions loaded, therefore 
PHPStart32 API allows for custom php.ini.

```
test0001.rpgle:

       DCL-C PHP_INI CONST(PHP_HOME+
       /share/vlang/php/php.ini');

       PHPStart32(PHP_INI:37:819);
```

Tip: If possible, start your RPG program via shell (example run above), expect much faster start times.



### VLANG/VPHP *SRVPGM APIs
```
====
/copy vphp_h
====
       // *************************************************
       // PHP API -- start 32bit php
       // If PASE not running, start PHP 32bit using 
       // default script (chat.php). PHP script is expected 
       // to issue ile_connect(), releasing control to RPG.
       //   ini       (optional) - custom php.ini
       //   ileCCSID  (optional) - override ILE job CCSID
       //   paseCCSID (optional) - override ILE job CCSID
       // *************************************************
       dcl-pr PHPStart32 IND;
         ini CHAR(65500) VALUE options (*nopass);
         ileCCSID INT(10) VALUE options (*nopass);
         paseCCSID INT(10) VALUE options (*nopass);
       end-pr;

       // *************************************************
       // PHP API -- start 32bit php
       // If PASE not running, start PHP 32bit using 
       // script. PHP script is expected to issue ile_connect(), 
       // releasing control to RPG.
       //   script               - PHP /path/script
       //   ini       (optional) - custom php.ini
       //   ileCCSID  (optional) - override ILE job CCSID
       //   paseCCSID (optional) - override ILE job CCSID
       // *************************************************
       dcl-pr PHPStartMain32 IND;
         script CHAR(65500) VALUE;
         ini CHAR(65500) VALUE options (*nopass);
         ileCCSID INT(10) VALUE options (*nopass);
         paseCCSID INT(10) VALUE options (*nopass);
       end-pr;

       // *************************************************
       // PHP API -- call/pass php script
       // PHP eval script chunk.
       //   scriptChunk          - script chunk for PHP eval
       // *************************************************
       dcl-pr PHPScript32 IND;
         scriptChunk CHAR(65500) VALUE;
       end-pr;

       // *************************************************
       // PHP API -- stop php
       // Stop PHP.
       // *************************************************
       dcl-pr PHPStop;
       end-pr;

====
/copy virt_h
====
       // *************************************************
       // virtual table API -- add your ILE call to table
       // Add your program procedure for PHP ile_callback().
       //   iName                - ILE procedure name (or PASE)
       //   iCall                - address %paddr(proc_name)
       //   pCall                - address function via PASE
       // *************************************************
       dcl-pr VirtualAddCallBack IND;
         iName CHAR(1024) VALUE;
         iCall POINTER(*PROC) VALUE;
         pCall POINTER VALUE options (*nopass);
       end-pr;

       // *************************************************
       // virtual table API -- add param description
       // Describe parms of program procedure 
       // for PHP ile_callback().
       //   iName                - ILE procedure name
       //   iType                - parm type (see virt_h)
       //   iLen                 - parm length
       //   iScale               - parm scale
       //   iTypeIO              - parm in, out, both
       // *************************************************
       dcl-pr VirtualAddCallBackDesc IND;
         iName CHAR(1024) VALUE;
         iType INT(10) VALUE; 
         iLen INT(10) VALUE; 
         iScale INT(10) VALUE;
         iTypeIO INT(10) VALUE;
       end-pr;

       // *************************************************
       // virtual table API -- Complete description
       // Complete or freeze procedure description.
       //   iName                - ILE procedure name
       // *************************************************
       dcl-pr VirtualCompleteCallBack IND;
         iName CHAR(1024) VALUE;
       end-pr;

       // *************************************************
       // virtual table -- ILE types
       // *************************************************
       DCL-C VIRT_CHAR CONST(1);
       DCL-C VIRT_VARCHAR CONST(2);
       DCL-C VIRT_USC2 CONST(3);
       DCL-C VIRT_VARUSC2 CONST(4);
       DCL-C VIRT_GRAPH CONST(5);
       DCL-C VIRT_VARGRAPH CONST(6);
       DCL-C VIRT_IND CONST(7);
       DCL-C VIRT_PACKED CONST(8);
       DCL-C VIRT_ZONED CONST(9);
       DCL-C VIRT_BINDEC CONST(10);
       DCL-C VIRT_INT3 CONST(11);
       DCL-C VIRT_INT5 CONST(12);
       DCL-C VIRT_INT10 CONST(13);
       DCL-C VIRT_LONG CONST(13);
       DCL-C VIRT_IN20 CONST(14);
       DCL-C VIRT_UNS3 CONST(15);
       DCL-C VIRT_UNS5 CONST(16);
       DCL-C VIRT_UNS10 CONST(17);
       DCL-C VIRT_UNS20 CONST(18);
       DCL-C VIRT_FLOAT4 CONST(19);
       DCL-C VIRT_FLOAT8 CONST(20);
       DCL-C VIRT_DOUBLE CONST(20);
       DCL-C VIRT_DATE CONST(21);
       DCL-C VIRT_TIME CONST(22);
       DCL-C VIRT_TIMESTAMP CONST(23);
       DCL-C VIRT_POINTER CONST(24);
       DCL-C VIRT_POINTERPROC CONST(25);
       DCL-C VIRT_STRING CONST(26);

       // *************************************************
       // virtual table -- ILE parm in, out, or both
       // *************************************************
       DCL-C VIRT_IO_IN CONST(1);
       DCL-C VIRT_IO_OUT CONST(2);
       DCL-C VIRT_IO_BOTH CONST(3);

       // *************************************************
       // virtual table API -- call PASE
       // This functin is usually wrapped by the scripting
       // language API (vphp_h PHPScript32), therefore
       // paseName(s) supplied by the langauge extension.
       // Howvere, in PASE mode, may be called directly.
       //   paseName             - PASE procedure name (key)
       //   scriptChunk          - string or script
       // *************************************************
       dcl-pr  VirtualCallScript32 IND;
         paseName CHAR(1024) VALUE;
         scriptChunk CHAR(65500) VALUE;
       end-pr;
```


### Technical big picture
Binding RPG SRVPGM (VLANG/VPHP), API PHPStart32(), as name suggests, 
starts PHP with a 'initial' or  'main' script (chat.php). RPG starts the PHP main script (chat.php), 
which, is responsible for calling ile_connect() to return control back to RPG. After ile_connect(), 
PHP is no longer in control, instead, your RPG program bound to VLANG/VPHP will direct the actions of 
PHP sending "chunks" of PHP scripts to be processed via 
RPG API PHPScript32('$myvar="hi there php, you are under RPG control";'). To be clear, 
PHPScript32('more php') is essentially adding more PHP code to 'main' PHP script  (chat.php), 
allowing you to change variables in the script, add/include PHP functions, and so on. 
The default start PHPStart32(), will always start chat.php, which contains ile_connect() to return control to RPG. 

```
VLANG/VPHP:
       DCL-C PHP_ARG1_CHAT CONST(PHP_HOME+
       '/share/vlang/php/chat.php');
       PHPStart32(*BLANKS:37:819);
       PHPScript32('$myvar="hi there php from RPG";')

chat.php:
error_log("chat.php we are returning to ile ...", 0);
ile_connect(); /* we are not coming back __RETURN back to ILE */

``` 

Start script chat.php is nothing special. In fact, testjson example, we replaced chat.php with jasonile.php 
adding more PHP functions at start time. Replacing chat.php with your own context mystart.php is recommended, 
PHPStartMain32('mystart.php'), thereby you have to write less RPG code, and, fewer calls to PHPScript32('more php').

```
testjson.rpgle:

       DCL-C JSON_ARG1 CONST(PHP_HOME+
       '/share/vlang/php/jsonile.php');

       PHPStartMain32(JSON_ARG1:*BLANKS:37:819);
```

Warning, deamon RPG programs (RPG CGIs), or RPG called repeatedly (SRVPGMs, etc.), 
adding or including PHP functions via dynamic PHPScript32('include "morephp.php";'), 
often leads to duplicate function names (see php.log). Duplicate function names 
kills PHP and your RPG hybrid program fails. However, you can work around 
duplicate function issues by using a simple exist test to see if the function was
previously included by your RPG program.

``` 
testjson.rpgle:

       PHPScript32(
         'if (function_exists("ile_decode") == FALSE) {' + LF
       + ' include '+PHP_HOME+'"/share/vlang/php/jsonile.php";' + LF
       + '}' + LF);
```

Hint: Personally, i think function_exists('myfunc') is tedious.
I much prefer including all PHP files in start replacement for chat.php, mystart.php, 
plus adding ile_connect() at the bottom of PHP script mystart.php.

PHP script side,  pecl extension ile.so , only two functions are exported for your script, 
ile_connect() giving control to ILE (your RPG program), and, ile_callback("RPGFuntionName", parms ...). 
PHP ile_callback("RPGName",parms...), as name suggests, calls back into your RPG program with script variables listed, 
thereby completing VLANG plumbing of passing variables between two sides RPG<parms>PHP, 
in the same process, using the same memory (original goal).

```
jsonile.php:

function ile_decode($a) {
  foreach ($a as $key => $value) {
    if (is_array($value)) {
      ile_decode($value);
    } else {
      ile_callback("JsonArray","$key","$value");
    }
  }
}

function ile_json_parse($a) {
  ile_decode(json_decode($a, true));
}

ile_connect(); /* we are not coming back __RETURN back to ILE */
```

Your RPG program, 'registers' your callback location before calling PHP, 
including the RPG function name and description of up to 32 pass-by-reference parameters.
Parameters are pass-by-reference (by pointer), therefore can be in, out, or both (in/out), 
wherein, variable content changes during call on either side. 
All RPG<>PHP conversions ASCII<>EBCDIC, real<>packed, real<>zoned, etc, 
are handled by VLANG/VPHP SRVPGM 
(if we missed some conversions, feel free to add via pull request).

```
testjson.rpgle:

       VirtualAddCallBack('JsonArray':%paddr(JsonArray));
       VirtualAddCallBackDesc('JsonArray':VIRT_CHAR:512:0:VIRT_IO_IN);
       VirtualAddCallBackDesc('JsonArray':VIRT_CHAR:1024:0:VIRT_IO_IN);
       VirtualCompleteCallBack('JsonArray');

       dcl-proc JsonArray export;
         dcl-pi *N;
          key CHAR(512);
          value CHAR(1024);
         end-pi;

         if first = *OFF;
           talk = %trim(talk) + ',';
         endif;
         talk = %trim(talk) 
              + '"' + %trim(key) + '"'
              + ':'
              + '"' + %trim(value) + '"';
         first = *OFF;

       end-proc; 
```

Included test examples mostly use one registered VirtualAddCallBack for example simplicity. 
In real application, you may have many different RPG 
VirtualAddCallBack("RPGFunc1"), ..., VirtualAddCallBack("RPGFunc4096"),
and, matching PHP 
ile_callback("RPGFunc1"), ..., ile_callback("RPGFunc4096").


```
VLANG/VPHP(virt_h.rpgle):

       DCL-C VIRT_TABLE_MAX CONST(4096);
```


### PHP ###

I am using ADC *SECOFR because not all IBM i profiles are authorized to use php, 
therefore tests fail. This is not a recommended way to enable RPG usage of php, you will 
need traditional security administrative action on php directories to enable your profile (i was lazy).


### php.log ####
When things go wrong, check the php errorlog.
```
bash-4.3$ tail $PHP_HOME/var/log/php.log 
[05-Apr-2016 14:38:12 America/Atikokan] PHP Warning:  ile_connect(): ...
[05-Apr-2016 14:38:12 America/Atikokan] PHP Warning:  ile_connect(): ...
```

