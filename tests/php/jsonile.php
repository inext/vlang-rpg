<?php
$ile = extension_loaded ('ile');
if (!$ile) {
  error_log("ile extension not loaded.", 0);
}

function ile_decode($a) {
  if (!is_array($a)) {
    error_log("jsonile.php json bad ..." . var_export($a), 0);
    ile_callback("JsonArray","error"," " . var_export($a) . " ");
    return;
  }
  foreach ($a as $key => $value) {
    if (is_array($value)) {
      ile_decode($value);
    } else {
      ile_callback("JsonArray","$key","$value");
    }
  }
}

function ile_json_parse($a) {
  ile_decode(json_decode($a, true));
}

ile_connect(); /* we are not coming back __RETURN back to ILE */

?>

