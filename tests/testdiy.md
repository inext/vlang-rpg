# testdiy.rpgle #
Sample testdiy is a RPG program calling PASE to pass various parameters types. 
In this test, passing common types between PASE and RPG callback.
This test uses the raw argv style callback from PASE.
RPG program must do all the conversion work (whew).
Note: This test does NOT start PHP, but use VLANG/VPHP.
I should create another *SRVPGM (VLANG/VPASE), but i was lazy,
and, srvpgm name really does not matter, PHP is not started.

```
testargv.c:

int _ile_call_here(char * data) {
:
	rc = _ile_callback_argv(name, argc, argv);
:
}

int main(int argc, char *argv[]) {
	_ile_srvpgm("VLANG/VPHP");
    _ile_pase("hey_ile", (void *)_ile_call_here);
    _ile_pase("cat_ile", (void *)_ile_call_here_again);
	_ile_connect(); /* return without exit */
}


testdiy.rpgle
       // diy -- manly programming, no easy description map
       // diy -- raw callback, no description, no convert
       // diy -- (chasing performance pennies ... cough)
       VirtualAddCallBack('IleCallMe':%paddr(IleCallMe));
       VirtualAddCallBack('IleStrAdd':%paddr(IleStrAdd));

       // *************************************************
       // callback location
       // *************************************************
       dcl-proc IleCallMe export;
         dcl-pi *N;
          meChar100 CHAR(100);
          meFloat8 FLOAT(8);
          meChar50 POINTER value options(*string);
          meInt INT(10);
         end-pi;
         dcl-s myBuff char(1000) inz(*BLANKS);
         dcl-s myOut char(1000) inz(*BLANKS);

         dcl-s ileCCSID INT(10) inz(0);
         dcl-s paseCCSID INT(10) inz(0);

         dcl-s rc INT(10) inz(0);
         DCL-S origLen INT(10) INZ(0);
         DCL-S buffLen INT(10) INZ(0);
         DCL-S outLen INT(10) INZ(0);
         DCL-S origPtr POINTER INZ(*NULL);
         DCL-S buffPtr POINTER INZ(*NULL);
         DCL-S outPtr POINTER INZ(*NULL);

         // TBD set the CCSID
         ileCCSID = 37;
         paseCCSID = 819;

         // DIY -- ebcdic-2-ascii
         myBuff = *BLANKS;
         myBuff = 'hi from ILE RPG free.';
         origPtr = %addr(meChar100);
         origLen = strlen(meChar100);
         buffPtr = %addr(myBuff);
         buffLen = %len(%trim(myBuff));
         outPtr = %addr(myOut);
         outLen = %size(myOut);
         memset(outPtr:0:outLen);
         rc = convCCSID(ileCCSID:paseCCSID:buffPtr:buffLen:outPtr:outLen);
         // diy - copy into pase storage, only as big "as is/was"
         cpybytes(origPtr:outPtr:origLen);

         // diy - float8 is double
         meFloat8 = 4.1;

         // DIY -- ebcdic-2-ascii
         myBuff = *BLANKS;
         myBuff = 'VLANG';
         origPtr = meChar50;
         origLen = strlen(meChar50);
         buffPtr = %addr(myBuff);
         buffLen = %len(%trim(myBuff));
         outPtr = %addr(myOut);
         outLen = %size(myOut);
         memset(outPtr:0:outLen);
         rc = convCCSID(ileCCSID:paseCCSID:buffPtr:buffLen:outPtr:outLen);
         // diy - copy into pase storage, only as big "as is/was"
         cpybytes(origPtr:outPtr:origLen);

         // diy - int is int
         meInt = 1;

       end-proc; 
```

### Source code ###
```
rpg/testdiy.rpgle
php/testhello.c
```

### Run ###
```
call qp2term
> system "call vlang/testdiy"
```

