     H AlwNull(*UsrCtl)

      /copy ../../vphp/iconf_h
      /copy ../../vphp/virt_h
      /copy ../../vphp/vos_h
      /copy ../../vphp/vphp_h

       // *************************************************
       // internal
       // *************************************************
       dcl-pr writeStdOutLF;
        text pointer value;
        len int(10) value;
       end-pr;

       dcl-pr IleCallMe;
          meChar100 CHAR(100);
          meFloat8 FLOAT(8);
          meChar50 POINTER value options(*string);
          meInt INT(10);
          meZoned ZONED(7:4);
          mePacked PACKED(12:2);
       end-pr;

       //**************************************************
       // test worked
       //**************************************************
       dcl-s iscallme ind inz(*OFF);

       // *************************************************
       // main program
       // *************************************************
       dcl-s msg1 CHAR(30) inz('starting PHP');
       dcl-s msg2 CHAR(30) inz('calling helloile.php');
       dcl-s msg3 CHAR(30) inz('good PHP');
       dcl-s msg4 CHAR(30) inz('fail PHP');
       dcl-s pgm CHAR(65500) inz(*BLANKS);
       dcl-s ini CHAR(65500) inz(*BLANKS);

       writeStdOutLF(%addr(msg1):%len(%trim(msg1)));
       // PHPStart32(*BLANKS:37:819);
       ini = PHP_SHARE 
           + '/' + PHP_VLANG 
           + '/' + 'php.ini';
       PHPStart32(ini:37:819);
       VirtualAddCallBack('IleCallMe':%paddr(IleCallMe));
       VirtualAddCallBackDesc('IleCallMe':VIRT_CHAR:100:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_FLOAT8:0:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_STRING:50:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_INT10:0:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_ZONED:7:4:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_PACKED:12:2:VIRT_IO_BOTH);
       VirtualCompleteCallBack('IleCallMe');
       writeStdOutLF(%addr(msg2):%len(%trim(msg2)));
       pgm = 'include "'
           + PHP_SHARE 
           + '/' + PHP_VLANG 
           + '/' + 'helloile.php";';
       PHPScript32(pgm);
       PHPStop();
       if iscallme = *ON;
         writeStdOutLF(%addr(msg3):%len(%trim(msg3)));
       else;
         writeStdOutLF(%addr(msg4):%len(%trim(msg4)));
       endif;
       return;

       // *************************************************
       // PHP callback location
       // *************************************************
       dcl-proc IleCallMe export;
         dcl-pi *N;
          meChar100 CHAR(100);
          meFloat8 FLOAT(8);
          meChar50 POINTER value options(*string);
          meInt INT(10);
          meZoned ZONED(7:4);
          mePacked PACKED(12:2);
         end-pi;
         dcl-s mylang char(50) inz(ILELIB);

         meChar100 = 'hi from ILE RPG free.';
         meFloat8 = 4.1;
         memset(meChar50:0:51);
         cpybytes(meChar50:%addr(mylang):%len(%trim(mylang)));
         meInt = 1;
         meZoned = 234.5678;
         mePacked = 321.21;

         iscallme = *ON;

       end-proc; 

       // *************************************************
       // write stdout with LF 
       // *************************************************
       dcl-proc writeStdOutLF export;
       dcl-pi *N;
        text pointer value;
        len int(10) value;
       end-pi;
       dcl-s addLF char(1) inz(x'25');

       writeIFS(1:text:len);
       writeIFS(1:%addr(addLF):1);

       end-proc; 


