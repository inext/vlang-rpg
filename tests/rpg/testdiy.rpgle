     H AlwNull(*UsrCtl)

      /copy ../../vphp/iconf_h
      /copy ../../vphp/virt_h
      /copy ../../vphp/vos_h
      /copy ../../vphp/vpase_h
      /copy ../../vphp/vlic_h
      /copy ../../vphp/vconv_h

       // *************************************************
       // internal
       // *************************************************
       dcl-pr writeStdOutLF;
        text pointer value;
        len int(10) value;
       end-pr;

       dcl-pr IleCallMe;
          meChar100 CHAR(100);
          meFloat8 FLOAT(8);
          meChar50 POINTER value options(*string);
          meInt INT(10);
       end-pr;

       dcl-pr IleStrAdd;
          meChar100 CHAR(100);
       end-pr;

       //**************************************************
       // test worked
       //**************************************************
       dcl-s iscallme ind inz(*OFF);
       dcl-s isstradd ind inz(*OFF);

       // *************************************************
       // main program
       // *************************************************
       dcl-s msg1 CHAR(60) inz('starting PASE testargv');
       dcl-s msg2 CHAR(60) inz('calling testargv _ile_call_here');
       dcl-s msg3 CHAR(60) inz('done');
       dcl-s msg4 CHAR(60) inz('fail callme');
       dcl-s msg5 CHAR(60) inz('fail stradd');
       DCL-S rcb IND INZ(*OFF);
       dcl-s pgm CHAR(65500) inz(*BLANKS);
       dcl-s arg CHAR(65500) dim(8) inz(*BLANKS);
       dcl-s env CHAR(65500) dim(8) inz(*BLANKS);
       dcl-s myPaseCCSID INT(10) inz(819);

       writeStdOutLF(%addr(msg1):%len(%trim(msg1)));

       pgm = PHP_SHARE 
           + '/' + PHP_VLANG 
           + '/' + 'testargv32';
       arg(1) = 'testargv32';
       env(1) = PHP_PATH 
              + ':' + PASE_PATH;
       env(2) = PHP_LIBPATH 
              + ':' + PASE_LIBPATH;
       env(3) = PASE_ATTACH;


       writeStdOutLF(%addr(pgm):%len(%trim(pgm)));

       rcb = PaseStart32(pgm:arg:env:myPaseCCSID);

       // diy -- manly programming, no easy description map
       // diy -- raw callback, no description, no convert
       // diy -- (chasing performance pennies ... cough)
       VirtualAddCallBack('IleCallMe':%paddr(IleCallMe));
       VirtualAddCallBack('IleStrAdd':%paddr(IleStrAdd));

       writeStdOutLF(%addr(msg2):%len(%trim(msg2)));

       rcb = VirtualCallScript32('hey_ile':'nothing');
       rcb = VirtualCallScript32('cat_ile':'RPG start');

       PaseStop();

       if iscallme = *ON and isstradd = *ON;
         writeStdOutLF(%addr(msg3):%len(%trim(msg3)));
       else;
         if iscallme = *OFF;
           writeStdOutLF(%addr(msg4):%len(%trim(msg4)));
         endif;
         if isstradd = *OFF;
           writeStdOutLF(%addr(msg5):%len(%trim(msg5)));
         endif;
       endif;

       return;

       // *************************************************
       // callback location
       // *************************************************
       dcl-proc IleCallMe export;
         dcl-pi *N;
          meChar100 CHAR(100);
          meFloat8 FLOAT(8);
          meChar50 POINTER value options(*string);
          meInt INT(10);
         end-pi;
         dcl-s myBuff char(1000) inz(*BLANKS);
         dcl-s myOut char(1000) inz(*BLANKS);

         dcl-s ileCCSID INT(10) inz(0);
         dcl-s paseCCSID INT(10) inz(0);

         dcl-s rc INT(10) inz(0);
         DCL-S origLen INT(10) INZ(0);
         DCL-S buffLen INT(10) INZ(0);
         DCL-S outLen INT(10) INZ(0);
         DCL-S origPtr POINTER INZ(*NULL);
         DCL-S buffPtr POINTER INZ(*NULL);
         DCL-S outPtr POINTER INZ(*NULL);

         // TBD set the CCSID
         ileCCSID = 37;
         paseCCSID = 819;

         // DIY -- ebcdic-2-ascii
         myBuff = *BLANKS;
         myBuff = 'hi from ILE RPG free.';
         origPtr = %addr(meChar100);
         origLen = strlen(meChar100);
         buffPtr = %addr(myBuff);
         buffLen = %len(%trim(myBuff));
         outPtr = %addr(myOut);
         outLen = %size(myOut);
         memset(outPtr:0:outLen);
         rc = convCCSID(ileCCSID:paseCCSID:buffPtr:buffLen:outPtr:outLen);
         // diy - copy into pase storage, only as big "as is/was"
         cpybytes(origPtr:outPtr:origLen);

         // diy - float8 is double
         meFloat8 = 4.1;

         // DIY -- ebcdic-2-ascii
         myBuff = *BLANKS;
         myBuff = 'VLANG';
         origPtr = meChar50;
         origLen = strlen(meChar50);
         buffPtr = %addr(myBuff);
         buffLen = %len(%trim(myBuff));
         outPtr = %addr(myOut);
         outLen = %size(myOut);
         memset(outPtr:0:outLen);
         rc = convCCSID(ileCCSID:paseCCSID:buffPtr:buffLen:outPtr:outLen);
         // diy - copy into pase storage, only as big "as is/was"
         cpybytes(origPtr:outPtr:origLen);

         // diy - int is int
         meInt = 1;

         iscallme = *ON;

       end-proc; 

       // *************************************************
       // callback location
       // *************************************************
       dcl-proc IleStrAdd export;
         dcl-pi *N;
          meChar100 CHAR(100);
         end-pi;
         dcl-s catme char(1000) inz(*BLANKS);

         dcl-s myBuff char(1000) inz(*BLANKS);
         dcl-s myOut char(1000) inz(*BLANKS);

         dcl-s ileCCSID INT(10) inz(0);
         dcl-s paseCCSID INT(10) inz(0);

         dcl-s rc INT(10) inz(0);
         DCL-S origLen INT(10) INZ(0);
         DCL-S buffLen INT(10) INZ(0);
         DCL-S outLen INT(10) INZ(0);
         DCL-S origPtr POINTER INZ(*NULL);
         DCL-S buffPtr POINTER INZ(*NULL);
         DCL-S outPtr POINTER INZ(*NULL);

         // TBD set the CCSID
         ileCCSID = 37;
         paseCCSID = 819;

         // concat buffer
         outPtr = %addr(catme);
         outLen = %size(catme);
         memset(outPtr:0:outLen);

         // pase provided string buffer, DIY agreement 
         // with pase testargv, 100 characters
         // of allocated PASE memory. 
         // yep-r-doodle, we trust each other. 
         // what could possibly go wrong?
         origPtr = %addr(meChar100);
         origLen = 100;

         // DIY -- already in ascii
         strcat(%addr(catme):origPtr);

         // DIY -- ebcdic-2-ascii
         myBuff = *BLANKS;
         myBuff = ' RPG free';
         buffPtr = %addr(myBuff);
         buffLen = 9;
         outPtr = %addr(myOut);
         outLen = %size(myOut);
         memset(outPtr:0:outLen);
         rc = convCCSID(ileCCSID:paseCCSID:buffPtr:buffLen:outPtr:outLen);
         strcat(%addr(catme):outPtr);

         // diy - copy into pase storage, only as big "as is/was"
         cpybytes(origPtr:%addr(catme):origLen);

         isstradd = *ON;

       end-proc; 

       // *************************************************
       // write stdout with LF 
       // *************************************************
       dcl-proc writeStdOutLF export;
       dcl-pi *N;
        text pointer value;
        len int(10) value;
       end-pi;
       dcl-s addLF char(1) inz(x'25');

       writeIFS(1:text:len);
       writeIFS(1:%addr(addLF):1);

       end-proc; 


