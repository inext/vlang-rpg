     H AlwNull(*UsrCtl)

      /copy ../../vphp/iconf_h
      /copy ../../vphp/virt_h
      /copy ../../vphp/vos_h
      /copy ../../vphp/vphp_h

       // *************************************************
       // internal
       // *************************************************
       dcl-pr writeStdOutLF;
        text pointer value;
        len int(10) value;
       end-pr;

       dcl-pr IleJaMe;
          meChar100 CHAR(100);
       end-pr;

       //**************************************************
       // test worked
       //**************************************************
       dcl-s iscallme ind inz(*OFF);

       // *************************************************
       // main program
       // *************************************************
       dcl-s msg1 CHAR(30) inz('starting PHP');
       dcl-s msg2 CHAR(30) inz('calling watson_trans.php');
       dcl-s msg3 CHAR(30) inz('good PHP');
       dcl-s msg4 CHAR(30) inz('fail PHP');
       dcl-s pgm CHAR(65500) inz(*BLANKS);
       dcl-s ini CHAR(65500) inz(*BLANKS);
       dcl-s enChar100 CHAR(100) inz(*BLANKS);
       dcl-s jaChar100 CHAR(100) inz(*BLANKS);

       writeStdOutLF(%addr(msg1):%len(%trim(msg1)));

       ini = PHP_SHARE 
           + '/' + PHP_VLANG 
           + '/' + 'php.ini';
       PHPStart32(ini:37:819);
       // PHPStart32(*BLANKS:37:819);
       VirtualAddCallBack('IleJaMe':%paddr(IleJaMe));
       VirtualAddCallBackDesc('IleJaMe':VIRT_CHAR:100:0:VIRT_IO_BOTH);
       VirtualCompleteCallBack('IleJaMe');

       writeStdOutLF(%addr(msg2):%len(%trim(msg2)));

       enChar100 = 'This is a test';
       pgm = 
             '$english="'+enChar100+'";'
           + '$model="en-ja";'
           + 'include "'
           + PHP_SHARE 
           + '/' + PHP_VLANG 
           + '/' + 'watson_trans.php";';
       PHPScript32(pgm);
       PHPStop();

       if iscallme = *ON;
         writeStdOutLF(%addr(enChar100):%len(%trim(enChar100)));
         writeStdOutLF(%addr(jaChar100):%len(%trim(jaChar100)));
         writeStdOutLF(%addr(msg3):%len(%trim(msg3)));
       else;
         writeStdOutLF(%addr(msg4):%len(%trim(msg4)));
       endif;
       return;

       // *************************************************
       // PHP callback location
       // *************************************************
       dcl-proc IleJaMe export;
       dcl-pi *N;
        meChar100 CHAR(100);
       end-pi;

       jaChar100 = meChar100;         

       iscallme = *ON;

       end-proc; 

       // *************************************************
       // write stdout with LF 
       // *************************************************
       dcl-proc writeStdOutLF export;
       dcl-pi *N;
        text pointer value;
        len int(10) value;
       end-pi;
       dcl-s addLF char(1) inz(x'25');

       writeIFS(1:text:len);
       writeIFS(1:%addr(addLF):1);

       end-proc; 


