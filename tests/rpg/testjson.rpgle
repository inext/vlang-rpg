     H AlwNull(*UsrCtl)

      /copy ../../vphp/iconf_h
      /copy ../../vphp/virt_h
      /copy ../../vphp/vos_h
      /copy ../../vphp/vphp_h
      /copy ../../vphp/vpase_h

       // *************************************************
       // global (see testjson.md)
       // *************************************************
       dcl-c NULLTERM const(x'00');
       dcl-c CRLF const(x'15');
       dcl-c LF const(x'25');
       dcl-c MAXJSON const(65500);

       dcl-pr ap_unescape_url INT(10) EXTPROC('ap_unescape_url');
         url POINTER VALUE options(*string);
       end-pr;

       dcl-pr ebcdic_unescape_url INT(10) EXTPROC('ebcdic_unescape_url');
         url POINTER VALUE options(*string);
       end-pr;

       // *************************************************
       // internal
       // *************************************************
       dcl-pr writeStdOutLF;
        text pointer value;
        len int(10) value;
       end-pr;

       dcl-pr JsonArray;
          key CHAR(512);
          value CHAR(1024);
       end-pr;

       dcl-pr zeroScript;
       end-pr;

       dcl-pr addScript;
         chunk char(MAXJSON) VALUE;
         debug ind VALUE;
       end-pr;

       dcl-pr callScript;
       end-pr;


       // *************************************************
       // main
       // *************************************************
       dcl-s rCopy POINTER inz(*NULL);
       dcl-s cMethod char(20) inz(*BLANKS);
       dcl-s cContent char(20) inz(*BLANKS);
       dcl-s pContent POINTER inz(*NULL);
       dcl-s szContent int(10) inz(0);
       dcl-s rTot int(10) inz(0);
       dcl-s rSz int(10) inz(0);
       dcl-s rc int(10) inz(0);
       dcl-s rcb ind inz(*OFF);
       dcl-s first ind inz(*ON);

       dcl-s json_script char(MAXJSON) inz(*BLANKS);
       dcl-s json_input char(MAXJSON) inz(*BLANKS);
       dcl-s talk char(MAXJSON) inz(*BLANKS);

       DCL-S outPtr POINTER INZ(*NULL);
       DCL-S outLen INT(10) INZ(0);

       dcl-s pgm CHAR(65500) inz(*BLANKS);

       // ----------
       // remove *debug php.log for production
       // ----------
       dcl-s isDebug ind inz(*OFF);

       pgm = PHP_SHARE 
           + '/' + PHP_VLANG 
           + '/' + 'jsonile.php';

       // ----------
       // output headers
       // ----------
       talk = *BLANKS;
       talk =
         'Content-type: text/plain' 
         + CRLF
         + CRLF
         + NULLTERM;
       outPtr = %addr(talk);
       outLen = strlen(outPtr);
       writeStdOutLF(outPtr:outLen);

       // --------
       // web request (json_input)
       // --------
       memset(%addr(json_input):0:MAXJSON);
       szContent = 0;
       pContent = %addr(json_input);
       rCopy = getenv('REQUEST_METHOD');
       if rCopy <> *NULL;
         cMethod = %str(rCopy:strlen(rCopy));
       endif;
       if cMethod='POST';
         rCopy = getenv('CONTENT_LENGTH');
         cContent = %str(rCopy:strlen(rCopy));
         szContent= %int(cContent);
         if szContent > 0;
           rTot = 0;
           rSz = 1;
           dou rTot >= szContent or rSz <= 0;
             rSz = readIFS(0:pContent+rTot:szContent-rTot);
             rTot += rSz;
           enddo;
         endif;
       elseif cMethod='GET';
         rCopy = getenv('QUERY_STRING');
         if rCopy <> *NULL;
           szContent = strlen(rCopy);
           strcat(pContent:rCopy);
         endif;
       endif;
       if szContent > 0;
         pContent = %addr(json_input);
         rc = ebcdic_unescape_url(pContent);
       endif;

       // ----------
       // php callback
       // ----------
       talk = '{';
       first = *ON;
       VirtualAddCallBack('JsonArray':%paddr(JsonArray));
       VirtualAddCallBackDesc('JsonArray':VIRT_CHAR:512:0:VIRT_IO_IN);
       VirtualAddCallBackDesc('JsonArray':VIRT_CHAR:1024:0:VIRT_IO_IN);
       VirtualCompleteCallBack('JsonArray');

       // ----------
       // start php (if needed)
       // ----------
       PHPStartMain32(pgm:*BLANKS:37:819);
       zeroScript();
       addScript('ile_json_parse(''':isDebug);
       if szContent > 0;
         addScript(json_input:*OFF);
       else;
         addScript('{"ok":"failure","error":"missing input"}':*OFF);
       endif;
       addScript(''');':*OFF);
       callScript();

       // ----------
       // output body
       // ----------
       talk = %trim(talk)
         + '}'
         + LF
         + NULLTERM;
       outPtr = %addr(talk);
       outLen = strlen(outPtr);
       writeStdOutLF(outPtr:outLen);

       // ----------
       // CGI, do not stop PHP
       // ----------
       // PHPStop();

       return;


       // *************************************************
       // PHP callback location
       // *************************************************
       dcl-proc JsonArray export;
         dcl-pi *N;
          key CHAR(512);
          value CHAR(1024);
         end-pi;

         if first = *OFF;
           talk = %trim(talk) + ',';
         endif;
         talk = %trim(talk) 
              + '"' + %trim(key) + '"'
              + ':'
              + '"' + %trim(value) + '"';
         first = *OFF;

       end-proc; 

       // *************************************************
       // PHP build my script
       // *************************************************
       dcl-proc zeroScript;
         dcl-pi *N;
         end-pi;
         memset(%addr(json_script):0:MAXJSON);
       end-proc; 

       dcl-proc addScript;
         dcl-pi *N;
           chunk char(MAXJSON) VALUE;
           debug ind VALUE;
         end-pi;
         dcl-s chunk2 char(MAXJSON) inz(*BLANKS);
         if debug = *ON;
           chunk2 = 
             '/* *debug php.log */' + LF
           + %trim(chunk)
           + NULLTERM;
         else;
           chunk2 = 
             %trim(chunk)
           + NULLTERM;
         endif;
         strcat(%addr(json_script):%addr(chunk2));
       end-proc; 

       dcl-proc callScript;
         dcl-pi *N;
         end-pi;
         PHPScript32(json_script);
       end-proc; 

       // *************************************************
       // write stdout with LF 
       // *************************************************
       dcl-proc writeStdOutLF export;
       dcl-pi *N;
        text pointer value;
        len int(10) value;
       end-pi;
       dcl-s addLF char(1) inz(x'25');

       writeIFS(1:text:len);
       writeIFS(1:%addr(addLF):1);

       end-proc; 

