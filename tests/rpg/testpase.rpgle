     H AlwNull(*UsrCtl)

      /copy ../../vphp/iconf_h
      /copy ../../vphp/virt_h
      /copy ../../vphp/vos_h
      /copy ../../vphp/vpase_h
      /copy ../../vphp/vlic_h
      /copy ../../vphp/vconv_h

       // *************************************************
       // internal
       // *************************************************
       dcl-pr writeStdOutLF;
        text pointer value;
        len int(10) value;
       end-pr;

       dcl-pr IleCallMe;
          meChar100 CHAR(100);
          meFloat8 FLOAT(8);
          meChar50 POINTER value options(*string);
          meInt INT(10);
          meZoned ZONED(7:4);
          mePacked PACKED(12:2);
       end-pr;

       dcl-pr IleStrAdd;
          meChar100 CHAR(100);
       end-pr;

       // *************************************************
       // main program
       // *************************************************
       dcl-s msg1 CHAR(60) inz('starting PASE testhello');
       dcl-s msg2 CHAR(60) inz('calling testhello _ile_call_here');
       dcl-s msg3 CHAR(60) inz('done');
       DCL-S rcb IND INZ(*OFF);
       dcl-s pgm CHAR(65500) inz(*BLANKS);
       dcl-s arg CHAR(65500) dim(8) inz(*BLANKS);
       dcl-s env CHAR(65500) dim(8) inz(*BLANKS);
       dcl-s myPaseCCSID INT(10) inz(819);

       writeStdOutLF(%addr(msg1):%len(%trim(msg1)));

       pgm = PHP_SHARE 
           + '/' + PHP_VLANG 
           + '/' + 'testhello32';
       arg(1) = 'testhello32';
       env(1) = PHP_PATH 
              + ':' + PASE_PATH;
       env(2) = PHP_LIBPATH 
              + ':' + PASE_LIBPATH;
       env(3) = PASE_ATTACH;

       rcb = PaseStart32(pgm:arg:env:myPaseCCSID);

       VirtualAddCallBack('IleCallMe':%paddr(IleCallMe));
       VirtualAddCallBackDesc('IleCallMe':VIRT_CHAR:100:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_FLOAT8:0:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_STRING:50:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_INT10:0:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_ZONED:7:4:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_PACKED:12:2:VIRT_IO_BOTH);
       VirtualCompleteCallBack('IleCallMe');

       VirtualAddCallBack('IleStrAdd':%paddr(IleStrAdd));
       VirtualAddCallBackDesc('IleStrAdd':VIRT_CHAR:100:0:VIRT_IO_BOTH);
       VirtualCompleteCallBack('IleStrAdd');

       writeStdOutLF(%addr(msg2):%len(%trim(msg2)));

       rcb = VirtualCallScript32('hey_ile':'nothing');
       rcb = VirtualCallScript32('cat_ile':'RPG start');

       PaseStop();

       writeStdOutLF(%addr(msg3):%len(%trim(msg3)));

       return;

       // *************************************************
       // callback location
       // *************************************************
       dcl-proc IleCallMe export;
         dcl-pi *N;
          meChar100 CHAR(100);
          meFloat8 FLOAT(8);
          meChar50 POINTER value options(*string);
          meInt INT(10);
          meZoned ZONED(7:4);
          mePacked PACKED(12:2);
         end-pi;
         dcl-s mylang char(50) inz(ILELIB);

         meChar100 = 'hi from ILE RPG free.';
         meFloat8 = 4.1;
         memset(meChar50:0:51);
         cpybytes(meChar50:%addr(mylang):%len(%trim(mylang)));
         meInt = 1;
         meZoned = 234.5678;
         mePacked = 321.21;

       end-proc; 

       // *************************************************
       // callback location
       // *************************************************
       dcl-proc IleStrAdd export;
         dcl-pi *N;
          meChar100 CHAR(100);
         end-pi;
         dcl-s catme char(1000) inz(*BLANKS);

         catme = meChar100;
         meChar100 = %trim(catme) + ' RPG free';

       end-proc; 

       // *************************************************
       // write stdout with LF 
       // *************************************************
       dcl-proc writeStdOutLF export;
       dcl-pi *N;
        text pointer value;
        len int(10) value;
       end-pi;
       dcl-s addLF char(1) inz(x'25');

       writeIFS(1:text:len);
       writeIFS(1:%addr(addLF):1);

       end-proc; 


