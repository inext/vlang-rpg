     H AlwNull(*UsrCtl)

      /copy ../../vphp/iconf_h
      /copy ../../vphp/virt_h
      /copy ../../vphp/vos_h
      /copy ../../vphp/vphp_h

       // *************************************************
       // internal
       // *************************************************
       dcl-pr writeStdOutLF;
        text pointer value;
        len int(10) value;
       end-pr;

       dcl-pr YahooFood;
          title CHAR(25);
          addr CHAR(25);
       end-pr;

       DCL-C MAX_ID CONST(100);
       dcl-ds food_t qualified template; 
          title CHAR(25);
          addr CHAR(25);
       end-ds;
       dcl-ds food likeds(food_t) dim(MAX_ID);
       dcl-s count int(10) inz(0);

       // *************************************************
       // main program
       // *************************************************
       dcl-c LF const(x'25');
       dcl-s i int(10) inz(0);
       dcl-s food_script char(4096) inz(*BLANKS);
       dcl-s food_rec char(52) inz(*BLANKS);

       PHPStart32(*BLANKS:37:819);
       VirtualAddCallBack('YahooFood':%paddr(YahooFood));
       VirtualAddCallBackDesc('YahooFood':VIRT_CHAR:25:0:VIRT_IO_IN);
       VirtualAddCallBackDesc('YahooFood':VIRT_CHAR:25:0:VIRT_IO_IN);
       VirtualCompleteCallBack('YahooFood');

       food_script =
         '$zip="55906";'
       + '$food="chinese";'
       + 'include "'
       + PHP_SHARE 
       + '/' + PHP_VLANG 
       + '/yahoofood.php";';

       PHPScript32(food_script);

       for i = 1 to MAX_ID;
         if food(i).title = *BLANKS;
           leave;
         endif;
         food_rec = %trim(food(i).title)
                    + ' -- '
                    + %trim(food(i).addr)
                    + LF;
         writeStdOutLF(%addr(food_rec):%len(%trim(food_rec)));
         // dsply food_rec;
       endfor;

       PHPStop();
       return;

       // *************************************************
       // PHP callback location
       // *************************************************
       dcl-proc YahooFood export;
         dcl-pi *N;
          title CHAR(25);
          addr CHAR(25);
         end-pi;
         dcl-s i int(10) inz(0);

         // copy out yahoo search records
         if count + 1 > MAX_ID;
           return;
         endif;
         count += 1;
         food(count).title = title;
         food(count).addr = addr;

       end-proc; 

       // *************************************************
       // write stdout with LF 
       // *************************************************
       dcl-proc writeStdOutLF export;
       dcl-pi *N;
        text pointer value;
        len int(10) value;
       end-pi;
       dcl-s addLF char(1) inz(x'25');

       writeIFS(1:text:len);
       writeIFS(1:%addr(addLF):1);

       end-proc; 


