     H AlwNull(*UsrCtl)

      /copy ../../vphp/iconf_h
      /copy ../../vphp/virt_h
      /copy ../../vphp/vos_h
      /copy ../../vphp/vphp_h

       // *************************************************
       // internal
       // *************************************************
       dcl-pr writeStdOutLF;
        text pointer value;
        len int(10) value;
       end-pr;

       dcl-pr MySqlAnimal;
          id CHAR(5);
          breed CHAR(20);
          name CHAR(15);
          weight CHAR(10);
       end-pr;

       DCL-C MAX_ID CONST(100);
       dcl-ds animal_t qualified template; 
          id CHAR(5);
          breed CHAR(20);
          name CHAR(15);
          weight CHAR(10);
       end-ds;
       dcl-ds animal likeds(animal_t) dim(MAX_ID);
       dcl-s count int(10) inz(0);

       // *************************************************
       // main program
       // *************************************************
       dcl-c LF const(x'25');
       dcl-s mysql_script char(4096) inz(*BLANKS);
       dcl-s id int(10) inz(0);
       dcl-s animal_rec char(52) inz(*BLANKS);

       PHPStart32(*BLANKS:37:819);
       VirtualAddCallBack('MySqlAnimal':%paddr(MySqlAnimal));
       VirtualAddCallBackDesc('MySqlAnimal':VIRT_CHAR:5:0:VIRT_IO_IN);
       VirtualAddCallBackDesc('MySqlAnimal':VIRT_CHAR:20:0:VIRT_IO_IN);
       VirtualAddCallBackDesc('MySqlAnimal':VIRT_CHAR:15:0:VIRT_IO_IN);
       VirtualAddCallBackDesc('MySqlAnimal':VIRT_CHAR:10:0:VIRT_IO_IN);
       VirtualCompleteCallBack('MySqlAnimal');

       mysql_script =
         '$limit=4;'
       + 'include "'
       + PHP_SHARE 
       + '/' + PHP_VLANG 
       + '/mysqlanimal.php";';

       PHPScript32(mysql_script);

       for id = 1 to MAX_ID;
         if animal(id).name = *BLANKS;
           leave;
         endif;
         animal_rec = %trim(animal(id).id)
                    + ' '
                    + %trim(animal(id).breed)
                    + ' '
                    + %trim(animal(id).name)
                    + ' '
                    + %trim(animal(id).weight)
                    + LF;
         writeStdOutLF(%addr(animal_rec):%len(%trim(animal_rec)));
         // dsply animal_rec;
       endfor;

       PHPStop();
       return;

       // *************************************************
       // PHP callback location
       // *************************************************
       dcl-proc MySqlAnimal export;
         dcl-pi *N;
          id CHAR(5);
          breed CHAR(20);
          name CHAR(15);
          weight CHAR(10);
         end-pi;
         dcl-s i int(10) inz(0);

         // copy out mysql fetch records
         if count + 1 > MAX_ID;
           return;
         endif;
         count += 1;
         animal(count).id = id;
         animal(count).breed = breed;
         animal(count).name = name;
         animal(count).weight = weight;

       end-proc; 

       // *************************************************
       // write stdout with LF 
       // *************************************************
       dcl-proc writeStdOutLF export;
       dcl-pi *N;
        text pointer value;
        len int(10) value;
       end-pi;
       dcl-s addLF char(1) inz(x'25');

       writeIFS(1:text:len);
       writeIFS(1:%addr(addLF):1);

       end-proc; 


