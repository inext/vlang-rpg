# testmysql.rpgle #
Sample testmysql is a RPG program calling PHP to access mysql database. In this test, accessing mysql table animals desired limit records ($limit). 

```
RPG (set php variables, include script):
       mysql_script =
         '$limit=4;'
       + 'include "share/vlang/php/mysqlanimal.php";';
       PHPScript32(mysql_script);

PHP (mysqlanimal.php):
$sql = "select id, breed, name, weight from animals limit {$limit}";
$result = mysql_query($sql, $link);
:
while ($row = mysql_fetch_array($result)) {
 ile_callback("MySqlAnimal", $row['id'], $row['breed'], $row['name'], $row['weight']);  
}

RPG (callback):
       dcl-proc MySqlAnimal export;
         dcl-pi *N;
          id CHAR(5);
          breed CHAR(20);
          name CHAR(15);
          weight CHAR(10);
         end-pi;
```

Test is using ZendDBI (MySql), shipped with Zend Srever for IBM i.

### Source code ###
```
rpg/testmysql.rpgle
php/yahoofood.php
```

### Configuration ###
We need to create user and schema mysql database. You may use call qp2term, or, shh me@myibmi, or, any other PASE command line for the administrative action in mysql (below).
```
$ export PATH=.:/usr/local/mysql/bin:/QOpenSys/usr/bin:/usr/ccs/bin:/QOpenSys/usr/bin/X11:/usr/sbin:/usr/bin
$ export LIBPATH=.:/usr/local/mysql/lib:/QOpenSys/usr/lib:/usr/lib
$ mysql --user root mysql
mysql> create database ranger;
mysql> CREATE USER 'ranger'@'localhost' IDENTIFIED BY 'ranger';
mysql> GRANT ALL PRIVILEGES ON * . * TO 'ranger'@'localhost';
mysql> FLUSH PRIVILEGES;
mysql> select host, user, password from mysql.user;
mysql> exit
$mysql -u ranger -p
mysql> exit
```

### Run ###
```
call qp2term
> cd  share/vlang
> system "call vlang/testmysql"
```

### PHP ###

I am using ADC *SECOFR is used because not all IBM i profiles are authorized to use php, therefore testmysql fails. This is not a recommended way to enable RPG usage of php, you will need traditional security administrative action on php directories to enable your profile (i was lazy).


### Debug ###
Always check php.log when things go wrong, saves many hours of stress.

```
bash-4.3$ tail var/log/php.log 
[05-Apr-2016 14:28:58 America/Atikokan] PHP Warning:  ile_connect():
```
