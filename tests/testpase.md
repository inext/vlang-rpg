# testpase.rpgle #
Sample testpase is a RPG program calling PASE to pass various parameters types. 
In this test, passing common types between PASE and RPG callback.
This test uses the SRVPGM built in conversion callback via 'descriptions'.
Note: This test does NOT start PHP, but use VLANG/VPHP.
I should create another *SRVPGM (VLANG/VPASE), but i was lazy,
and, srvpgm name really does not matter, 
PHP is not started.

```
testhello.c:

int _ile_call_here(char * data) {
:
	rc = _ile_callback(name, &desc[0], argc, argv);
:
}
int main(int argc, char *argv[]) {
	_ile_srvpgm("VLANG/VPHP");
    _ile_pase("hey_ile", (void *)_ile_call_here);
	_ile_connect(); /* return without exit */
}


testpase.rpgle:

       VirtualAddCallBack('IleCallMe':%paddr(IleCallMe));
       VirtualAddCallBackDesc('IleCallMe':VIRT_CHAR:100:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_FLOAT8:0:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_STRING:50:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_INT10:0:0:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_ZONED:7:4:VIRT_IO_BOTH);
       VirtualAddCallBackDesc('IleCallMe':VIRT_PACKED:12:2:VIRT_IO_BOTH);
       VirtualCompleteCallBack('IleCallMe');


       // *************************************************
       // callback location
       // *************************************************
       dcl-proc IleCallMe export;
         dcl-pi *N;
          meChar100 CHAR(100);
          meFloat8 FLOAT(8);
          meChar50 POINTER value options(*string);
          meInt INT(10);
          meZoned ZONED(7:4);
          mePacked PACKED(12:2);
         end-pi;
         dcl-s mylang char(50) inz('VLANG');

         meChar100 = 'hi from ILE RPG free.';
         meFloat8 = 4.1;
         memset(meChar50:0:51);
         cpybytes(meChar50:%addr(mylang):%len(%trim(mylang)));
         meInt = 1;
         meZoned = 234.5678;
         mePacked = 321.21;

       end-proc; 
```

### Source code ###
```
rpg/testpase.rpgle
php/testhello.c
```

### Run ###
```
call qp2term
> cd  share/vlang
> system "call vlang/testpase"
```

