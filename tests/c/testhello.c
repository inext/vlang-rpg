#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include "itest.h"
#include "virt400.h"

static char * meChar100 = "I am c code";
static double meFloat8 = 8;
static char * meChar50 = "Happy c code.";
static int meInt = 4;
static double meZoned = 22.22;
static double mePacked = 42.42;

static void _clear_all(virtVar_t desc[], char *argv[]) {
	int i = 0;
	for (i=0;i<VIRT_CALLARG_MAX;i++) {
		argv[i] = (char *) NULL;
        desc[i].iType = 0; 
		desc[i].iLen = 0; 
		desc[i].iScale = 0;
        desc[i].iTypeIO = VIRT_IO_IN;
        desc[i].paseAddr = 0;
	}
}

int _ile_call_here(char * data) {
	int i = 0;
	int rc = 0;
	int argc = 0;
	char * name = "IleCallMe";
	char *argv[VIRT_CALLARG_MAX];
	virtVar_t desc[VIRT_CALLARG_MAX];

	_clear_all(desc, argv);

	argc = 6;

	desc[0].iType = VIRT_STRING;
	desc[0].iLen = strlen(meChar100);
	desc[0].iScale = 0;
	desc[0].iTypeIO = VIRT_IO_BOTH;
	desc[0].paseAddr = 0;
	argv[0] = (char *) meChar100;

	desc[1].iType = VIRT_DOUBLE;
	desc[1].iLen = sizeof(meFloat8);
	desc[1].iScale = 0;
	desc[1].iTypeIO = VIRT_IO_BOTH;
	desc[1].paseAddr = 0;
	argv[1] = (char *) &meFloat8;

	desc[2].iType = VIRT_STRING;
	desc[2].iLen = strlen(meChar50);
	desc[2].iScale = 0;
	desc[2].iTypeIO = VIRT_IO_BOTH;
	desc[2].paseAddr = 0;
	argv[2] = (char *) meChar50;

	desc[3].iType = VIRT_LONG;
	desc[3].iLen = sizeof(meInt);
	desc[3].iScale = 0;
	desc[3].iTypeIO = VIRT_IO_BOTH;
	desc[3].paseAddr = 0;
	argv[3] = (char *) &meInt;

	desc[4].iType = VIRT_DOUBLE;
	desc[4].iLen = sizeof(meZoned);
	desc[4].iScale = 0;
	desc[4].iTypeIO = VIRT_IO_BOTH;
	desc[4].paseAddr = 0;
	argv[4] = (char *) &meZoned;

	desc[5].iType = VIRT_DOUBLE;
	desc[5].iLen = sizeof(mePacked);
	desc[5].iScale = 0;
	desc[5].iTypeIO = VIRT_IO_BOTH;
	desc[5].paseAddr = 0;
	argv[5] = (char *) &mePacked;

    printf("\nbefore %s %f %s %d %f %f\n", 
        meChar100,
        meFloat8,
        meChar50,
        meInt,
        meZoned,
        mePacked
        );

	rc = _ile_callback(name, &desc[0], argc, argv);

    printf("after %s %f %s %d %f %f\n", 
        (char *)desc[0].paseAddr,
        meFloat8,
        (char *)desc[2].paseAddr,
        meInt,
        meZoned,
        mePacked
        );

	for (i=0;i<argc;i++) {
		if (desc[i].paseAddr) {
			free((char *)desc[i].paseAddr);
		}
	}
}

int _ile_call_here_again(char * data) {
	int rc = 0, i = 0;
	int argc = 0;
	char * name = "IleStrAdd";
	char *argv[VIRT_CALLARG_MAX];
	virtVar_t desc[VIRT_CALLARG_MAX];
	char paseChar100[100];

	_clear_all(desc, argv);

	argc = 1;

    sprintf(paseChar100,"%s %s", data, meChar50);

	desc[0].iType = VIRT_STRING;
	desc[0].iLen = strlen(paseChar100);
	desc[0].iScale = 0;
	desc[0].iTypeIO = VIRT_IO_BOTH;
	desc[0].paseAddr = 0;
	argv[0] = (char *) paseChar100;

    printf("before %s\n", paseChar100);

	rc = _ile_callback(name, &desc[0], argc, argv);

    sprintf(paseChar100,"%s", (char *)desc[0].paseAddr);

    printf("after %s\n", paseChar100);

	for (i=0;i<argc;i++) {
		if (desc[i].paseAddr) {
			free((char *)desc[i].paseAddr);
		}
	}
}


int main(int argc, char *argv[]) {
	char libmbr[32];

	memset(libmbr,0,sizeof(libmbr));
    strcpy(libmbr,ILELIB);
    strcat(libmbr,"/VPHP");

	_ile_srvpgm(libmbr);
    _ile_pase("hey_ile", (void *)_ile_call_here);
    _ile_pase("cat_ile", (void *)_ile_call_here_again);
	_ile_connect(); /* return without exit */
}
