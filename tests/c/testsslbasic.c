/* reference http://www.ibm.com/developerworks/library/l-openssl/ */
#include "openssl/ssl.h"
#include "openssl/bio.h"
#include "openssl/err.h"

#include "itest.h"
#include "stdio.h"
#include "string.h"
#include "virt400.h"

static void _clear_all(virtVar_t desc[], char *argv[]) {
	int i = 0;
	for (i=0;i<VIRT_CALLARG_MAX;i++) {
		argv[i] = (char *) NULL;
        desc[i].iType = 0; 
		desc[i].iLen = 0; 
		desc[i].iScale = 0;
        desc[i].iTypeIO = VIRT_IO_IN;
        desc[i].paseAddr = 0;
	}
}

int _ile_respond(char * data) {
	int rc = 0, i = 0;
	int argc = 0;
	char * name = "IleSSL";
	char *argv[VIRT_CALLARG_MAX];
	virtVar_t desc[VIRT_CALLARG_MAX];

	_clear_all(desc, argv);

	argc = 1;
	desc[0].iType = VIRT_STRING;
	desc[0].iLen = 1024;
	desc[0].iScale = 0;
	desc[0].iTypeIO = VIRT_IO_IN;
	desc[0].paseAddr = 0;
	argv[0] = (char *) data;
	rc = _ile_callback(name, &desc[0], argc, argv);
}

int _ile_request(char * request) {
    BIO * bio;
    SSL * ssl;
    SSL_CTX * ctx;

    int p;

    char r[1024];
    char php_cacert[1024];

    /* Set up the library */
    SSL_library_init();

    ERR_load_BIO_strings();
    SSL_load_error_strings();
    OpenSSL_add_all_algorithms();

    /* Set up the SSL context */

    ctx = SSL_CTX_new(SSLv23_client_method());

    /* Load the trust store */
    /* wget https://curl.haxx.se/ca/cacert.pem */
    memset(php_cacert,0,sizeof(php_cacert));
    strcat(php_cacert,PHP_SHARE);
    strcat(php_cacert,"/");
    strcat(php_cacert,PHP_VLANG);
    strcat(php_cacert,"/");
    strcat(php_cacert,"cacert.pem");
    if(! SSL_CTX_load_verify_locations(ctx, php_cacert, NULL))
    {
        fprintf(stderr, "Error loading trust store\n");
        ERR_print_errors_fp(stderr);
        SSL_CTX_free(ctx);
        return 0;
    }

    /* Setup the connection */

    bio = BIO_new_ssl_connect(ctx);

    /* Set the SSL_MODE_AUTO_RETRY flag */

    BIO_get_ssl(bio, & ssl);
    SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);

    /* Create and setup the connection */

    BIO_set_conn_hostname(bio, "www.verisign.com:https");

    if(BIO_do_connect(bio) <= 0)
    {
        fprintf(stderr, "Error attempting to connect\n");
        ERR_print_errors_fp(stderr);
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return 0;
    }

    /* Check the certificate */

    if(SSL_get_verify_result(ssl) != X509_V_OK)
    {
        fprintf(stderr, "Certificate verification error: %i\n", SSL_get_verify_result(ssl));
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return 0;
    }

    /* Send the request */

    BIO_write(bio, request, strlen(request));

    /* Read in the response */

    for(;;)
    {
        p = BIO_read(bio, r, 1023);
        if(p <= 0) break;
        r[p] = 0;
#ifdef PASE_CHECK_SSL
        printf("%s", r);
#else
        _ile_respond(r);
#endif
    }

    /* Close the connection and free the context */

    BIO_free_all(bio);
    SSL_CTX_free(ctx);
    return 0;
}

int main(int argc, char *argv[]) {
	char libmbr[32];
    char * request2 = "GET / HTTP/1.1\nHost: www.verisign.com\nConnection: Close\n\n";

#ifdef PASE_CHECK_SSL
    _ile_request(request2);
#else
	memset(libmbr,0,sizeof(libmbr));
    strcpy(libmbr,ILELIB);
    strcat(libmbr,"/VPHP");

	_ile_srvpgm(libmbr);
    _ile_pase("hey_ssl", (void *)_ile_request);
	_ile_connect();
#endif
}
