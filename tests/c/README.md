# README_PASE #

VLANG can be use without a scripting language (README_PASE.md), allowing calls to
most anything PASE. The trick is to create a PASE main using libvirt400.a 
function _ile_connect, to return PASE control to your RPG program without
actually exiting PASE (testpase). You can add many other shared libraries, calling
function in normal PASE c API, then provide a callback mapping for your 
RPG program (testssl).

```
testhello.c:

int _ile_call_here(char * data) {
:
	rc = _ile_callback(name, &desc[0], argc, argv);
:
}
int main(int argc, char *argv[]) {
	_ile_srvpgm("VLANG/VPHP");
    _ile_pase("hey_ile", (void *)_ile_call_here);
	_ile_connect(); /* return without exit */
}
```

If you wish to chase "higher speed" performance, 
your RPG program can bypass SRVPGM built in conversions.
Your PASE program may pass raw argv style arguments to your RPG callback. 
Your RPG program must do all work ebcdic<>ascii, real<>packed, etc..
Do It Yourself, can be highest performing option (see testdiy.rpgle).

```
testargv.c:

int _ile_call_here(char * data) {
:
	rc = _ile_callback_argv(name, argc, argv);
:
}

int main(int argc, char *argv[]) {
	_ile_srvpgm("VLANG/VPHP");
    _ile_pase("hey_ile", (void *)_ile_call_here);
    _ile_pase("cat_ile", (void *)_ile_call_here_again);
	_ile_connect(); /* return without exit */
}
```

### How do I get set up? ###
See main README.md.

### Performance ###
Do not use a ILE/PASE debugger while running performance tests. We have learned by painful experience, 
debuggers greatly extend PASE start time.


### VLANG *SRVPGM APIs
```
====
/copy vpase_h
====

       // *************************************************
       // PASE API -- start 32bit
       // If PASE not running, start PASE 32bit using 
       // default spgm. pgm is expected 
       // to issue ile_connect(), releasing control to RPG.
       //   pgm                  - PASE program (/path/pgm)
       //   arg                  - upt to eight arguments
       //   env                  - upt to eight env vars
       //   paseCCSID (optional) - override ILE job CCSID
       // *************************************************
       dcl-pr PaseStart32 IND;
          pgm CHAR(65500) VALUE;
          arg CHAR(65500) dim(8) VALUE;
          env CHAR(65500) dim(8) VALUE;
          paseCCSID INT(10) VALUE;
       end-pr;

       // *************************************************
       // PASE API -- stop PASE
       // *************************************************
       dcl-pr PaseStop;
       end-pr;

====
/copy virt_h
====
       // *************************************************
       // virtual table API -- add your ILE call to table
       // Add your program procedure for PHP ile_callback().
       //   iName                - ILE procedure name (or PASE)
       //   iCall                - address %paddr(proc_name)
       //   pCall                - address function via PASE
       // *************************************************
       dcl-pr VirtualAddCallBack IND;
         iName CHAR(1024) VALUE;
         iCall POINTER(*PROC) VALUE;
         pCall POINTER VALUE options (*nopass);
       end-pr;

       // *************************************************
       // virtual table API -- add param description
       // Describe parms of program procedure 
       // for PHP ile_callback().
       //   iName                - ILE procedure name
       //   iType                - parm type (see virt_h)
       //   iLen                 - parm length
       //   iScale               - parm scale
       //   iTypeIO              - parm in, out, both
       // *************************************************
       dcl-pr VirtualAddCallBackDesc IND;
         iName CHAR(1024) VALUE;
         iType INT(10) VALUE; 
         iLen INT(10) VALUE; 
         iScale INT(10) VALUE;
         iTypeIO INT(10) VALUE;
       end-pr;

       // *************************************************
       // virtual table API -- Complete description
       // Complete or freeze procedure description.
       //   iName                - ILE procedure name
       // *************************************************
       dcl-pr VirtualCompleteCallBack IND;
         iName CHAR(1024) VALUE;
       end-pr;

       // *************************************************
       // virtual table -- ILE types
       // *************************************************
       DCL-C VIRT_CHAR CONST(1);
       DCL-C VIRT_VARCHAR CONST(2);
       DCL-C VIRT_USC2 CONST(3);
       DCL-C VIRT_VARUSC2 CONST(4);
       DCL-C VIRT_GRAPH CONST(5);
       DCL-C VIRT_VARGRAPH CONST(6);
       DCL-C VIRT_IND CONST(7);
       DCL-C VIRT_PACKED CONST(8);
       DCL-C VIRT_ZONED CONST(9);
       DCL-C VIRT_BINDEC CONST(10);
       DCL-C VIRT_INT3 CONST(11);
       DCL-C VIRT_INT5 CONST(12);
       DCL-C VIRT_INT10 CONST(13);
       DCL-C VIRT_LONG CONST(13);
       DCL-C VIRT_IN20 CONST(14);
       DCL-C VIRT_UNS3 CONST(15);
       DCL-C VIRT_UNS5 CONST(16);
       DCL-C VIRT_UNS10 CONST(17);
       DCL-C VIRT_UNS20 CONST(18);
       DCL-C VIRT_FLOAT4 CONST(19);
       DCL-C VIRT_FLOAT8 CONST(20);
       DCL-C VIRT_DOUBLE CONST(20);
       DCL-C VIRT_DATE CONST(21);
       DCL-C VIRT_TIME CONST(22);
       DCL-C VIRT_TIMESTAMP CONST(23);
       DCL-C VIRT_POINTER CONST(24);
       DCL-C VIRT_POINTERPROC CONST(25);
       DCL-C VIRT_STRING CONST(26);

       // *************************************************
       // virtual table -- ILE parm in, out, or both
       // *************************************************
       DCL-C VIRT_IO_IN CONST(1);
       DCL-C VIRT_IO_OUT CONST(2);
       DCL-C VIRT_IO_BOTH CONST(3);

       // *************************************************
       // virtual table API -- call PASE
       // This functin is usually wrapped by the scripting
       // language API (vphp_h PHPScript32), therefore
       // paseName(s) supplied by the langauge extension.
       // Howvere, in PASE mode, may be called directly.
       //   paseName             - PASE procedure name (key)
       //   scriptChunk          - string or script
       // *************************************************
       dcl-pr  VirtualCallScript32 IND;
         paseName CHAR(1024) VALUE;
         scriptChunk CHAR(65500) VALUE;
       end-pr;

```

### Technical big picture
See description in README_PHP.

