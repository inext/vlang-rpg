#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include "itest.h"
#include "virt400.h"

char * meChar100;
double * meFloat8;
char * meChar50;
int * meInt;

static void _clear_all(char *argv[]) {
	int i = 0;
	for (i=0;i<VIRT_CALLARG_MAX;i++) {
		argv[i] = (char *) NULL;
	}
}

int _ile_call_here(char * data) {
	int i = 0;
	int rc = 0;
	int argc = 0;
	char * name = "IleCallMe";
	char *argv[VIRT_CALLARG_MAX];

	argc = 4;

	argv[0] = (char *) meChar100;
	argv[1] = (char *) meFloat8;
	argv[2] = (char *) meChar50;
	argv[3] = (char *) meInt;

    printf("\nbefore %s %f %s %d\n", 
        meChar100,
        *meFloat8,
        meChar50,
        *meInt
        );

	rc = _ile_callback_argv(name, argc, argv);

    printf("after %s %f %s %d\n", 
        meChar100,
        *meFloat8,
        meChar50,
        *meInt
        );
}

int _ile_call_here_again(char * data) {
	int rc = 0, i = 0;
	int argc = 0;
	char * name = "IleStrAdd";
	char *argv[VIRT_CALLARG_MAX];
	char paseChar100[100];

	_clear_all(argv);

	argc = 1;

    sprintf(paseChar100,"%s %s", data, meChar50);

	argv[0] = (char *) paseChar100;

    printf("before %s\n", paseChar100);

	rc = _ile_callback_argv(name, argc, argv);

    printf("after %s\n", paseChar100);
}


int main(int argc, char *argv[]) {
	char libmbr[32];

	memset(libmbr,0,sizeof(libmbr));
    strcpy(libmbr,ILELIB);
    strcat(libmbr,"/VPHP");

	meChar100 = malloc(15);
	sprintf(meChar100,"I am c code");
	meFloat8 = malloc(10);
	*meFloat8 = 8;
	meChar50 = malloc(15);
	sprintf(meChar50,"Happy c code.");
	meInt = malloc(10);
	*meInt = 4;

	_ile_srvpgm(libmbr);
	_ile_pase("hey_ile", (void *)_ile_call_here);
	_ile_pase("cat_ile", (void *)_ile_call_here_again);
	_ile_connect(); /* return without exit */
}
