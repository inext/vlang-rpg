# testjson.rpgle #
Sample testjson is a RPG CGI program calling PHP.
 
An easy sample test for people trying to set-up "no password' web RPG CGI-slave-PHP
using Basic Authentication, Kerberos, LDAP, Identity mapper, etc. 
The example simply receives some json via http GET/POST. 
Decodes using php's json_decode with a php script callback into RPG function testjson JsonArray. 
The collected data is simply returned to the browser in json format. 
This RPG CGI sample testjson, nothing complex done with the data (intentional), 
simply allowing you to see RPG CGI working end-2-end without getting lost in 
irrelevant json action context (left to the student).

```
RPG (start pase, if needed)
       PHPStartMain32(JSON_ARG1:*BLANKS:37:819);
       zeroScript();
       addScript('ile_json_parse(''':isDebug);
       if szContent > 0;
         addScript(json_input:*OFF);
       else;
         addScript('{"ok":"failure","error":"missing input"}':*OFF);
       endif;
       addScript(''');':*OFF);
       callScript();

PHP (jsonile.php):
function ile_decode($a) {
  foreach ($a as $key => $value) {
    if (is_array($value)) {
      ile_decode($value);
    } else {
      ile_callback("JsonArray","$key","$value");
    }
  }
}

function ile_json_parse($a) {
  ile_decode(json_decode($a, true));
}
ile_connect(); /* we are not coming back __RETURN back to ILE */

RPG (callback):
       dcl-proc JsonArray export;
         dcl-pi *N;
          key CHAR(512);
          value CHAR(1024);
         end-pi;
```


### Source code ###
```
rpg/testjson.rpgle
php/jsonile.php
```

### Run ###
```
http://myibmi/vlang/testjson.pgm?{%22frog%22%3A%22courtin%22,%22song%22%3A%22hum%22}
```

### Configuration ###
Demonstrating Basic Authentication in following configuration (see note UsedID ADC).

/www/apachedft/conf/httpd.conf
```
<Directory />       
   AuthType Basic
   AuthName "IBMi OS User Profile"
   Require valid-user
   PasswdFile %%SYSTEM%%
   UserID ADC
   Order Deny,Allow 
   Deny From all     
</Directory>     

<Directory /www/apachedft/htdocs>
  order allow,deny
  allow from all
</Directory>

ScriptAlias /vlang/ /QSYS.LIB/VLANG.LIB/
<Directory /QSYS.LIB/VLANG.LIB/>
  AllowOverride None
  order allow,deny
  allow from all
  SetHandler cgi-script
  Options +ExecCGI
</Directory>
```

Start and stop Apache.
```
ENDTCPSVR SERVER(*HTTP) INSTANCE(APACHEDFT)
strTCPSVR SERVER(*HTTP) INSTANCE(APACHEDFT)
```

Note:

UserID ADC *SECOFR is used because QTMHHTP1 is not authorized to use php, therefore testjson CGI fails. This is not a recommended way to enable RPG CGI usage of php, 
you will need traditional security administrative action on php directories to enable QTMHHTP1 (i was lazy).


### Debug ###
testjson.rpgle demostrates a new function in ile.so, '*debug'. 
Adding a '*debug' to a comment in your RPG generated php script will write partial script into php.log 

```
bash-4.3$ tail var/log/php.log 
[05-Apr-2016 14:28:58 America/Atikokan] PHP Warning:  ile_connect(): /* *debug -- php.log */ if (function_exists("ile_decode") == FALSE) 
```

